import { Component, Input, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AuthService } from 'ionic-appauth';
import * as Keycloak from 'keycloak-js';
import { CartService } from 'src/app/services/cart.service';
import { StorageService } from 'src/app/services/storage.service';
import { UserService } from 'src/app/services/user.service';
import { environment } from 'src/environments/environment';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss'],
})
export class HeaderComponent implements OnInit {

  // Variáveis
  @Input() backButton: boolean;
  @Input() closeButton: boolean;
  @Input() userButton: boolean;
  @Input() cartButton: boolean;
  @Input() title: string;

  constructor(
    // public ionKeycloak: IonicKeycloakService,
    public cartService: CartService,
    private route: Router,
    private auth: AuthService,
    public user: UserService
  ) {
    if (!environment.production){
      console.log('INICIOU O HEADER');
      console.log('O USUÁRIO ESTÁ LOGADO? ');
    }
  }

  async ngOnInit() {
  }


  perfil(){
    if (this.user.userData){
      this.route.navigate(['/perfil'])
    } else {
      this.auth.signIn();
    }
  }

  cart(){
    this.route.navigate(['/carrinho']);
  }

  close(){
    this.route.navigate(['/home']);
  }

}
