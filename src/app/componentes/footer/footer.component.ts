import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { InAppBrowser } from '@awesome-cordova-plugins/in-app-browser/ngx';
import { AuthService } from 'ionic-appauth';
import * as $ from 'jquery'
import { UserService } from 'src/app/services/user.service';

@Component({
  selector: 'app-footer',
  templateUrl: './footer.component.html',
  styleUrls: ['./footer.component.scss'],
})
export class FooterComponent implements OnInit {
  // Variáveis
  isMenuVisible: boolean = false;

  constructor(
    private route: Router,
    public user: UserService,
    public auth: AuthService,
    private iab: InAppBrowser,
  ) { }

  ngOnInit() {}

  toggleMenu(){
    if (this.user.userData){
      if (!this.isMenuVisible){
        this.isMenuVisible = true;
        $('#menu').css('margin-bottom', '70px');
      } else {
        this.isMenuVisible = false;
        $('#menu').css('margin-bottom', '0px')
      }
    } else {
      this.auth.signIn();
    }

  }

  goToPerfil(){
    if (this.user.userData){
      this.route.navigate(['/perfil'])
    } else {
      this.auth.signIn();
    }
  }

  goToExternalLink(link){
    this.iab.create(link, '_self', 'hideurlbar=true,hidenavigationbuttons=true').show();
  }

}
