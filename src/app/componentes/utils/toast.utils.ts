import { Injectable } from '@angular/core';
import { ToastController } from '@ionic/angular';


@Injectable()
//Provê o serviço de Toast em toda a aplicação
export class ToastProvider {

  constructor(public toast: ToastController) {

  }

  async showToast(mensagem: string, tempo?: number) {
    const toast = await this.toast.create({
      message: mensagem,
      mode: 'ios',
      duration: tempo || 2500
    });
    toast.present();
  }

  async showToastLogin(mensagem: string, tempo?: number, color?:string) {
    const toast = await this.toast.create({
      message: mensagem,
      mode: 'ios',
      duration: tempo || 2500,
      color: color || 'danger'
    });
    toast.present();
  }

}