import { Injectable } from '@angular/core';
import { AlertController } from '@ionic/angular';


@Injectable()
//Provê o serviço de Toast em toda a aplicação
export class AlertProvider {

  constructor(public alert: AlertController) {

  }

  async showAlert(header, subheader, msg, buttons, backdrop?: boolean) {
    const alert = await this.alert.create({
      cssClass: 'alerta-fomed',
      header: header,
      subHeader: subheader,
      message: msg,
      buttons: buttons,
      mode: 'ios',
      backdropDismiss: backdrop || true,
      
    });

    await alert.present();
  }

}