import { leadingComment } from '@angular/compiler';
import { Injectable } from '@angular/core';
import { LoadingController } from '@ionic/angular';


@Injectable()
//Provê o serviço de Loading em toda a aplicação
export class LoadingPaymentProvider {

  loadAberto: boolean = false

  constructor(public load: LoadingController) {

  }

  async showLoading() {
    if (!this.loadAberto){
      this.loadAberto = true;
      const loading = await this.load.create({
      mode: 'ios',
      cssClass: 'global-loading',
      translucent: true,
      message: 'Processando Pagamento...',
    });
    await loading.present();
    setTimeout(() => {
      if (this.loadAberto){
        this.load.dismiss();
      }
    }, 120000);
    }
  }

  closeLoading(){
    if (this.loadAberto){
      this.load.dismiss();
    }
    this.loadAberto = false;
  }

}
