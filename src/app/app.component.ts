import { Component, NgZone } from '@angular/core';
import { StatusBar, Style } from '@capacitor/status-bar';
import { Device } from '@capacitor/device'
import { environment } from 'src/environments/environment';
import { CapacitorGoogleMaps } from '@capacitor-community/capacitor-googlemaps-native';
import { SplashScreen } from '@capacitor/splash-screen';
import { AuthService } from 'ionic-appauth';
import { StorageService } from './services/storage.service';
import { Platform } from '@ionic/angular';


@Component({
  selector: 'app-root',
  templateUrl: 'app.component.html',
  styleUrls: ['app.component.scss'],
})
export class AppComponent {
  phoneInfo: any = {};

  constructor(
    private auth: AuthService,
    private storage: StorageService,
    private platform: Platform,
  ) {
    this.getPhoneInfo();
    this.initializeApp();
    this.initializeGoogleMap();
    this.loggerEnv();
    this.platform.ready().then(async () => {
      this.storage.removeItem("EUM41S_US3R");
      if (this.phoneInfo.platform !== "web"){
        await StatusBar.hide();
        await StatusBar.setOverlaysWebView({overlay: false});
      }
    });
  }

  async initializeApp(){
    await this.auth.init();
    await SplashScreen.hide();
  }

  async getPhoneInfo(){
    this.phoneInfo = await Device.getInfo()
    if (!environment.production){
      console.log('Phone info: ', this.phoneInfo);
    }
  }

  async initializeGoogleMap(){
    if (this.phoneInfo.platform == "android" || this.phoneInfo.platform == "ios"){
      await CapacitorGoogleMaps.initialize({
        key: environment.gMaps.apiKey
      });
    } else {
      console.log("PLATFORM WEB DETECTED. GOOGLE MAPS WILL NOT WORK.");
    }
  }

  loggerEnv() {
    let test = environment.production ? "prod" : "dev";
    console.log("RODANDO EM: ", test);
  }

}
