
import { Injectable } from '@angular/core';
import { InAppBrowserObject, InAppBrowserOptions } from '@awesome-cordova-plugins/in-app-browser';
import { InAppBrowser } from '@awesome-cordova-plugins/in-app-browser/ngx';
import { Capacitor } from '@capacitor/core';
import { Browser } from 'ionic-appauth';

@Injectable()
export class CustomCapacitorBrowser extends Browser {
  tabOpen: any = false;

  constructor(
    private iab: InAppBrowser
  ){
    super();
  }
    public closeWindow(): void | Promise<void> {
      if(!this.tabOpen)
          throw new Error("Capacitor Browser Is Undefined!");

      if(Capacitor.getPlatform() !== 'android'){
          this.tabOpen.close();
      }
    }

    public async showWindow(url: string): Promise<string | undefined> {
      console.log("VEIO PARA O SHOW WINDOW");
      let options: InAppBrowserOptions = {
        zoom: 'no',
        fullscreen: 'yes',
        hidenavigationbuttons: 'yes',
      }
      this.tabOpen = this.iab.create(url, '_self', options).show();

      return ;
    }
}
