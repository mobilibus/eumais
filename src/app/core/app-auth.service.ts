import { Injectable } from '@angular/core';
import { Requestor, TokenResponse } from '@openid/appauth';
import { AuthService } from 'ionic-appauth';

@Injectable({
  providedIn: 'root'
})
export class AppAuthService {

  constructor(
    private auth: AuthService,
    private requestor: Requestor
  ) {

  }

  public async request(method: "GET" | "POST" | "PUT" | "DELETE", url: string, data?: any) {
    const token: TokenResponse = await this.auth.getValidToken();
    return this.requestor.xhr({
      url: url,
      method: method,
      data: JSON.stringify(data),
      headers: this.addHeaders(token),
    })
  }

  private addHeaders(token: any){
    return (token) ? {
      'Authorization': `${(token.tokenType === 'bearer') ? 'Bearer' : token.tokenType} ${token.accessToken}`,
      'Content-Type': 'application/json'
    } : {};
  }
}
