import { Platform } from '@ionic/angular';
import { Requestor, StorageBackend } from '@openid/appauth';
import { NgModule, NgZone } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Browser, AuthService } from 'ionic-appauth';
import { CapacitorBrowser, CapacitorSecureStorage } from 'ionic-appauth/lib/capacitor';
import { authFactory } from './factories';
import { HttpDataService } from './http-data.service';

@NgModule({
  imports: [
    CommonModule
  ],
  providers: [
    {
      provide: StorageBackend,
      useClass: CapacitorSecureStorage
    }, {
      provide: Requestor,
      useClass: HttpDataService
    }, {
      provide: Browser,
      useClass: CapacitorBrowser
    }, {
      provide: AuthService,
      useFactory: authFactory,
      deps: [Platform, NgZone, Requestor, Browser, StorageBackend]
    }
  ]
})
export class CoreModule { }
