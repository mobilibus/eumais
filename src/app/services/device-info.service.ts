import { Injectable } from '@angular/core';
import { Device } from '@capacitor/device';

@Injectable({
  providedIn: 'root'
})
export class DeviceInfoService {
  phoneInfo: any;

  constructor() { 
    this.getPhoneInfo();
  }

  async getPhoneInfo(){
    this.phoneInfo = await Device.getInfo();
  }
}
