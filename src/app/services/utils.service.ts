import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { UniqueDeviceID } from '@ionic-native/unique-device-id/ngx';

@Injectable({
  providedIn: 'root'
})
export class UtilsService {
  // Aqui ficarão algumas variáveis que são usadas poucas vezes.
  sectionals: any;
  uuid: string;

  constructor(
    private http: HttpClient
  ){
  }

  generalGetApi(url: any){
    const httpOptions = {
      headers: new HttpHeaders(
          {'Content-Type' : 'application/json'}
      )
    };
    return this.http.get<any>(url, httpOptions)
      .toPromise();
  }

  apenasNumeros(string){
      var numsStr = string.replace(/[^0-9]/g,'');
      return parseInt(numsStr);
  }


}
