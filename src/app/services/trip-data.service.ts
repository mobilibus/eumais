import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class TripDataService {
  tripSectionals: any = {};
  tripDates: any = {};
  tripHash: string;
  tripPassengersCount: number;
  tripSeatsSelecteds: any = [];
  tripUtilizers: any = [];
  itsMeSelected = false;
  tripWithReturn = false;
  // Após a seleção e reserva dos assentos
  tripSelectedSeatKey: string;
  tripIsReserved = false;




  constructor() { }

  cleanData(){
    this.tripSectionals = {};
    this.tripDates = {};
    this.tripHash = undefined
    this.tripPassengersCount = undefined;
    this.tripSeatsSelecteds = [];
    this.tripUtilizers = [];
    this.tripIsReserved = false;
    this.itsMeSelected = false;
    this.tripWithReturn = false;
  }
}
