import { Injectable } from '@angular/core';
import { StorageService } from './storage.service';

@Injectable({
  providedIn: 'root'
})
export class UserService {
  userData: any;
  userToken: string;

  constructor(
    private storage: StorageService
  ) { }

  setUser(data: any){
    this.userData = data;
    this.storage.setObjItem(this.storage.userKey, data);
  }

  setToken(token: string){
    this.userToken = token;
    this.storage.setTextItem(this.storage.tokenKey, token);
  }
}
