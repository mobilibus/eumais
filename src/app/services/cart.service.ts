import { Injectable } from '@angular/core';
import { TripDataService } from './trip-data.service';

@Injectable({
  providedIn: 'root'
})
export class CartService {
  // Variáveis
  data: any;
  items: any[] = [];

  constructor(
    private tripData: TripDataService
  ) { }

  addTripToCart(seat_arr: any, with_return: boolean = false){
    console.log('seat_arr:', seat_arr);

    seat_arr.map(seat=>{
      // adiciona à array do carrinho
      this.items.push({
        tripInfo: {
          tripSectionals: this.tripData.tripSectionals,
          tripDates: this.tripData.tripDates,
          tripHash: this.tripData.tripHash,
          tripPassengersCount: this.tripData.tripPassengersCount,
          seatInfo: seat,
          tripUtilizers: this.tripData.tripUtilizers,
          itsMeSelected: this.tripData.itsMeSelected,
          tripSelectedSeatKey: this.tripData.tripSelectedSeatKey,
          tripIsReserved: this.tripData.tripIsReserved
        },
        road: {
          direction: '0',
          selected_seat_key: seat.selected_seat_key,
          utilizer: seat.utilizer,
          with_return: with_return
        }
      });
    })
    console.log(this.items);
  }

  cleanCart(){
    this.items = [];
  }


}
