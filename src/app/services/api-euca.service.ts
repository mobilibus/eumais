import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { StorageService } from './storage.service';
import { UserService } from './user.service';

@Injectable()
export class ApiEucatur {
    server: string = 'https://api-app-eumais.eucatur.com.br/'; //prod
    // server: string = 'https://api-app-unstable.eumais.cf/'; //dev
    serverMon: string = environment.mobilibusApi;
    debug: boolean = true;

    constructor(
      private http: HttpClient,
      private user: UserService,
      private localStorage: StorageService
    ){
    }

    getData(url: string){
      var httpOptions;
        if (!environment.production){
            console.log('Req: [GET] efetuada: '+ url);
        }

        if (this.user.userData){
           httpOptions = {
            headers: new HttpHeaders(
                {
                  'Content-Type' : 'application/json',
                  'Authorization' : `${this.user.userToken}`,
                }
            )
          }
          console.log(this.user.userToken);
        } else {
          httpOptions = {
            headers: new HttpHeaders(
                {'Content-Type' : 'application/json'}
            )
          }
        }

        let fullURL = this.server + url;
        return this.http.get(fullURL, httpOptions)
            .toPromise();


    }

    postData(url: string, body: any){
      var httpOptions;

        if (!environment.production){
            console.log('Requisição API POST efetuada: '+ url);
            console.log(JSON.stringify(body));
        }

        if (this.user.userData){
          httpOptions = {
           headers: new HttpHeaders(
               {
                 'Content-Type' : 'application/json',
                 'Authorization' : `${this.user.userToken}`,
               }
           )
         }
         console.log(this.user.userToken);
       } else {
         const httpOptions = {
           headers: new HttpHeaders(
               {'Content-Type' : 'application/json'}
               )
         }
       }

        let fullURL = this.server + url;
        return this.http.post(fullURL, JSON.stringify(body), httpOptions)
          .toPromise();
    }

    deleteData(url: string){
      var httpOptions;

        if (!environment.production){
            console.log('Requisição API DELETE efetuada: '+ url);
        }

        if (this.user.userData){
          httpOptions = {
           headers: new HttpHeaders(
               {
                 'Content-Type' : 'application/json',
                 'Authorization' : `${this.user.userToken}`,
               }
           )
         }
         console.log(this.user.userToken);
       } else {
         httpOptions = {
           headers: new HttpHeaders(
               {'Content-Type' : 'application/json'}
               )
         }
       }

        let fullURL = this.server + url;
        return this.http.delete(fullURL, httpOptions)
          .toPromise();
    }

    editData(url: string, body: any){
      var httpOptions;

      if (!environment.production){
          console.log('Requisição API PUT efetuada: '+ url);
          console.log(JSON.stringify(body));
      }

      if (this.user.userData){
        httpOptions = {
         headers: new HttpHeaders(
             {
               'Content-Type' : 'application/json',
               'Authorization' : `${this.user.userToken}`,
             }
         )
       }
       console.log(this.user.userToken);
     } else {
       const httpOptions = {
         headers: new HttpHeaders(
             {'Content-Type' : 'application/json'}
             )
       }
     }

      let fullURL = this.server + url;
      return this.http.put(fullURL, JSON.stringify(body), httpOptions)
        .toPromise();
    }

    tripMonitoring(endpoint: string){
      const httpOptions = {
        headers: new HttpHeaders(
            {'Content-Type' : 'application/json'}
        )
      }
      return this.http.get<any>(`${this.serverMon}/${endpoint}`)
        .toPromise();
    }
}
