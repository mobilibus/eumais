import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class StorageService {
  tokenKey = '3UM41S4PPT0K3N';
  userKey = '3UM41SUS3R';
  expiresInKey = '3UM41S3XP1R3S';

  constructor() {
    if (this.getItem(this.userKey)) {
      this.removeItem(this.userKey);
    }

    if (this.getItem(this.tokenKey)) {
      this.removeItem(this.tokenKey);
    }

  }

  setTextItem(key:string, value:string){
    window.localStorage.setItem(key, value);
    if (!environment.production){
      console.log(key, value);
    }

  }

  setObjItem(key:string, value:any){
    window.localStorage.setItem(key, JSON.stringify(value));
  }

  removeItem(key:string){
    window.localStorage.removeItem(key);
  }

  getItem(key:string, isObj?:boolean){
    if (isObj){
      return JSON.parse(window.localStorage.getItem(key))
    } else {
      return window.localStorage.getItem(key);
    }
  }
}
