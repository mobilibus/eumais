import { Component, ElementRef, ViewChild } from '@angular/core';
import { Router } from '@angular/router';
import { CalendarComponentOptions } from 'ion2-calendar';
import { AlertProvider } from 'src/app/componentes/utils/alert.utils';
import { LoadingProvider } from 'src/app/componentes/utils/loading.utils';
import { ApiEucatur } from 'src/app/services/api-euca.service';
import { UtilsService } from 'src/app/services/utils.service';
import { environment } from 'src/environments/environment';
import { Keyboard } from '@capacitor/keyboard';
import { TripDataService } from 'src/app/services/trip-data.service';
import * as moment from 'moment';
import { InAppBrowser } from '@awesome-cordova-plugins/in-app-browser/ngx';
import { AuthActions, AuthService, IAuthAction } from 'ionic-appauth';
import { Subscription } from 'rxjs';
import { UserService } from 'src/app/services/user.service';
import { ActionPerformed, PushNotifications, PushNotificationSchema } from '@capacitor/push-notifications';


@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage {
  @ViewChild('inputPartida') inputPartida;
  @ViewChild('inputDestino') inputDestino;

  // Variáveis
  events$ = this.auth.events$;
  user$ = this.auth.user$;
  sub: Subscription;
  slideOpts = {
    initialSlide: 1,
    speed: 400
  };
  slideItems: any[] = [
    { image: './assets/imgs/slider/eumais_banner.png', url: 'https://www.eumaisviagens.com.br/' }
  ];
  dateRange: any = { from: '', to: '' };
  type: 'string'; // 'string' | 'js-date' | 'moment' | 'time' | 'object'
  optionsRange: CalendarComponentOptions = {
    pickMode: 'single',
    weekdays: ['DOM','SEG', 'TER', 'QUA', 'QUI', 'SEX', 'SÁB'],
    showMonthPicker: false,
  };
  showDateRange = false;
  ticketsNumber = 1;
  showListaPartida = false;
  showListaDestino = false;
  showCalendarIda = false;
  showCalendarVolta = false;
  sectionalsLoaded: any = [];
  sectionalsPartida: any = [];
  sectionalsDestino: any = [];
  sectionalsSelected: any = {};
  travelsFind: any = [];
  fieldPartida: string;
  fieldDestino: string;
  segmentValue = 1;

  constructor(
    private API: ApiEucatur,
    private utils: UtilsService,
    private route: Router,
    private alert: AlertProvider,
    private loading: LoadingProvider,
    private auth: AuthService,
    public user: UserService,
    public tripData: TripDataService,
    private iab: InAppBrowser,

  ) {
    moment.locale('pt-br')
  }

  ngOnInit(){
    this.loading.showLoading();
    this.sub = this.auth.events$.subscribe((action: any)=> this.onLoginSuccess(action))
    this.requestPermToPushNotifications();

  }

  ngOnDestroy() {
    this.sub.unsubscribe();
  }

  ionViewWillEnter(){
    if (!this.utils.sectionals){
      this.getSectionals();
    } else {
      this.sectionalsPartida = this.utils.sectionals;
      this.sectionalsDestino = this.utils.sectionals;
    }
    this.segmentValue = 2;
  }

  toggleCalendarIda(){
    if (this.showCalendarIda){
      this.showCalendarIda = false;
    } else {
      this.showCalendarIda = true;
    }
  }

  toggleCalendarVolta(){
    if (this.showCalendarVolta){
      this.showCalendarVolta = false;
    } else {
      this.showCalendarVolta = true;
    }
  }

  selectDateIda(){
    this.dateRange.from
    if (!environment.production) console.log(this.dateRange);
    this.toggleCalendarIda();
  }

  selectDateVolta(){
    this.dateRange.to
    if (!environment.production) console.log(this.dateRange);
    this.toggleCalendarVolta();
 }

  datetimeIda(e){
    this.dateRange.from = this.dateFormatter(e.detail.value);

    if (!environment.production){
      console.log(this.dateRange);
    }
  }

  datetimeVolta(e){
    this.dateRange.to = this.dateFormatter(e.detail.value);
    if (!environment.production){
      console.log(this.dateRange);
    }
  }

  getSectionals(){
    this.API.getData('sectionals?is_road_station=true')
      .then((response)=>{
        if (!environment.production) console.log(response);
        this.utils.sectionals = response;
        this.sectionalsPartida = response;
        this.sectionalsDestino = response;
        setTimeout(() => {
          this.loading.closeLoading();
        }, 300)

      })
      .catch((err)=>{
        if (!environment.production) console.log('DEU ERRO AQUI', err)
      });
  }

  onSearchPartida() {
    if (this.fieldPartida == ''){
      this.sectionalsPartida = this.utils.sectionals;
    } else {
      this.sectionalsPartida = this.searchStringInArray(this.fieldPartida, this.utils.sectionals)
    }

  }

  segmentChanged(e){
    this.segmentValue = e.detail.value;
    this.dateRange = {from : '', to: ''};
  }

  onSearchDestino() {
    if (this.fieldDestino == ''){
      this.sectionalsPartida = this.utils.sectionals;
    } else {
      this.sectionalsDestino = this.searchStringInArray(this.fieldDestino, this.utils.sectionals)
    }

  }

  toggleListaPartida(){
    if (!this.showListaPartida){
      this.showListaPartida = true;
    } else {
      this.showListaPartida = false;
      this.inputPartida.value = '';
      setTimeout(() => {
        Keyboard.hide();
      }, 30);

    }
  }

  toggleListaDestino(){
    if (!this.showListaDestino){
      this.showListaDestino = true;
    } else {
      this.showListaDestino = false;
      this.inputDestino.value = '';
      setTimeout(() => {
        Keyboard.hide();
      }, 30);
    }
  }

  selectPartida(sec){
    this.sectionalsSelected.from = sec;
    this.showListaPartida = false;
  }

  selectDestino(sec){
    this.sectionalsSelected.to = sec;
    this.showListaDestino = false;
  }

  toggleDateRange(){
    if (!this.showDateRange){
      this.showDateRange = true;
    } else {
      this.showDateRange = false;
    }
  }

  mngTickets(type: string){
    if (type == 'add'){
      this.ticketsNumber = this.ticketsNumber + 1;
    } else {
      if (this.ticketsNumber > 1){
        this.ticketsNumber = this.ticketsNumber - 1;
      }
    }
  }

  searchTravels(){
    if (!this.sectionalsSelected.from){
      this.alert.showAlert('Oops', 'Algo não foi informado...', `Você não informou a origem de sua viagem!`, ['Ok']);
    } else if (!this.sectionalsSelected.to){
      this.alert.showAlert('Oops', 'Algo não foi informado...', `Você não informou o destino de sua viagem!`, ['Ok']);
    } else if (!this.dateRange.from){
      this.alert.showAlert('Oops', 'Algo não foi informado...', `Você não informou a data de sua viagem!`, ['Ok']);
    } else {
      this.loading.showLoading();
      if (!environment.production){
        console.log('Pesquisando por:')
        console.log(`Partida: ${this.sectionalsSelected.from.description} - ${this.sectionalsSelected.from.uf_acronym}`);
        console.log(`Destino: ${this.sectionalsSelected.to.description} - ${this.sectionalsSelected.to.uf_acronym}`);
        console.log(`Passagens: ${this.ticketsNumber}`);
        console.log(`Data: ${this.dateRange.from} e ${this.dateRange.to}`);
      }

      this.API.getData(`road/travels/search?origin_sectional_id=${this.sectionalsSelected.from.id}&destiny_sectional_id=${this.sectionalsSelected.to.id}&departure_date=${this.dateRange.from}`)
        .then((response)=>{
          this.travelsFind = response;
          this.loading.closeLoading();
          if (this.travelsFind.length > 0){
            this.tripData.tripSectionals = { to: this.sectionalsSelected.to, from: this.sectionalsSelected.from };
            this.route.navigate([`/travel-results/${this.sectionalsSelected.from.id}/${this.sectionalsSelected.to.id}/1/${this.ticketsNumber}/${this.dateRange.from}/${this.dateRange.to}`])
          } else {
            this.alert.showAlert(`Poxa!`, '', `No momento não temos viagens disponíveis para o destino desejado. Fique tranquilo, logo teremos mais opções para você!`, ['Ok']);
          }
        })
    }
  }



  dateFormatter(date, type = 'EUA'){
    if (!environment.production){
      console.log(date);
    }

    let dayOfMonth = date.substr(8,2);
    let month = date.substr(5,2);
    let year = date.substr(0,4);

    if (type == 'BRL'){
      return `${dayOfMonth}/${month}/${year}`;
    } else {
      return `${year}-${month}-${dayOfMonth}`;
    }

  }

  searchStringInArray(str, arr) {
    let newArr = [];

    if (!environment.production){
      console.log('Pesquisando por: '+ str.toLowerCase());
    }

    arr.forEach(el => {
      if (el.description.toLowerCase().includes(str.toLowerCase())){
        newArr.push(el);
      }
    });
    return newArr;
  }

  openExternalLink(link){
    this.iab.create(link, '_self', 'hideurlbar=true,hidenavigationbuttons=true').show();
  }

  goToPerfil(){
    this.auth.signIn();
  }

  private onLoginSuccess(action: IAuthAction){
    if (action.action === AuthActions.SignInSuccess){
      this.getUserInfo();
      this.getUserToken();
      // this.navCtrl.navigateRoot('home');
    }
  }

  isLogged(){
    return false;
  }

  async getUserInfo(){
    await this.auth.loadUserInfo()
    this.auth.user$
        .subscribe((user)=>{
          this.user.setUser(user);
        });
  }

  async getUserToken(){
    await this.auth.token$
        .subscribe((token)=>{
          this.user.setToken(token.accessToken);
          console.log(this.user.userToken);
        });
  }

  requestPermToPushNotifications(){
    // Request permission to use push notifications
    // iOS will prompt user and return if they granted permission or not
    // Android will just grant without prompting
    PushNotifications.requestPermissions()
      .then((res)=>{
        PushNotifications.register();
      });

      // On success, we should be able to receive notifications
      PushNotifications.addListener('registration', (token)=>{
        console.log('Push registration success, token: '+ token.value);
      });

      // Some issue with our setup and push will not work
    PushNotifications.addListener('registrationError',
    (error: any) => {
      console.log('Notifications error: ', error);
    }
  );

    // Show us the notification payload if the app is open on our device
    PushNotifications.addListener('pushNotificationReceived',
      (notification: PushNotificationSchema) => {
        console.log('Push received: ' + JSON.stringify(notification));
      }
    );

    // Method called when tapping on a notification
    PushNotifications.addListener('pushNotificationActionPerformed',
      (notification: ActionPerformed) => {
        console.log('Push action performed: ' + JSON.stringify(notification));
      }
    );
  }

}
