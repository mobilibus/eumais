import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { SeatingMapPage } from './seating-map.page';

const routes: Routes = [
  {
    path: '',
    component: SeatingMapPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class SeatingMapPageRoutingModule {}
