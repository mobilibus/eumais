/* eslint-disable @typescript-eslint/naming-convention */

import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { AuthService } from 'ionic-appauth';
import { AlertProvider } from 'src/app/componentes/utils/alert.utils';
import { LoadingProvider } from 'src/app/componentes/utils/loading.utils';
import { ApiEucatur } from 'src/app/services/api-euca.service';
import { TripDataService } from 'src/app/services/trip-data.service';
import { UserService } from 'src/app/services/user.service';
import { environment } from 'src/environments/environment';
import { seatingMapMock } from './seating-map.mock';

@Component({
  selector: 'app-seating-map',
  templateUrl: './seating-map.page.html',
  styleUrls: ['./seating-map.page.scss'],
})
export class SeatingMapPage implements OnInit {
  // Variáveis
  travelKey: string;
  seatingMapReturned: any = [];
  remapedSeatMap: any = null;
  floorSelected = 1;
  selectedSeats = 0;
  seatsToSelect: number;

  constructor(
    private activatedRoute: ActivatedRoute,
    private loading: LoadingProvider,
    private API: ApiEucatur,
    private alert: AlertProvider,
    private route: Router,
    private auth: AuthService,
    private user: UserService,
    public tripData: TripDataService
  ) { }

  ngOnInit() {
    this.activatedRoute.params
    .subscribe(params =>{
      this.travelKey = params.travel_key;
      this.seatsToSelect = params.adults;
      console.log('Travel Key: '+this.travelKey);
      console.log(JSON.stringify(this.tripData));
    });
    if (this.travelKey == "0101"){
      this.seatingMapReturned = seatingMapMock[0];
    } else {
      this.loading.showLoading();
      this.getSeatingMap();
    }
  }

  getSeatingMap(){
    this.API.getData(`road/travels/${this.travelKey}/seating-map`)
      .then((response)=>{
        this.seatingMapReturned = response[0];
        console.log(this.seatingMapReturned);
        setTimeout(() => {
          this.loading.closeLoading();
        }, 300);
      });
  }

  floorSegment(e){
    if (!environment.production){
      console.log(e);
    }
  }

  selectSeat(seat){
    if (this.user.userData){
      if (!seat.isSelected){
        if (this.selectedSeats < this.seatsToSelect){
          seat.isSelected = true;
          this.selectedSeats = this.selectedSeats + 1;
        } else {
          this.alert.showAlert(
            'Oops', 'Você já selecionou...', `Parece que você já selecionou a quantidade de assentos que informou ao buscar a viagem.`,
            ['Ok']
          );
        }
      } else {
        seat.isSelected = false;
        this.selectedSeats = this.selectedSeats - 1;
      }
    } else {
      this.alert.showAlert('Oops','', 'Estamos felizes com seu pedido, mas para concluí-lo precisa estar logado(a). Realize seu login ou faça seu cadastro para continuar a compra.',
      [
        {
          text: 'Entrar',
          handler: () => this.auth.signIn()
        },
        'Cancelar'
      ]
      );
    }

  }

  goToPersonToSeat(){
    this.tripData.tripSeatsSelecteds = [];
    if (this.seatingMapReturned.floor_1){
      this.seatingMapReturned.floor_1.map(seat => {
        if (seat.isSelected){
          this.tripData.tripSeatsSelecteds.push({ travel_key: this.travelKey, amount: seat.amount, busy: seat.busy, category: seat.category, column: seat.column, isSelected: seat.isSelected,
            line: seat.line, number: seat.number, price_discount: seat.price_discount, tariff: seat.tariff, woman_space: seat.woman_space
           });
        }
      });
    }
    if (this.seatingMapReturned.floor_2){
      this.seatingMapReturned.floor_2.map(seat =>{
        if (seat.isSelected){
          this.tripData.tripSeatsSelecteds.push({ travel_key: this.travelKey, amount: seat.amount, busy: seat.busy, category: seat.category, column: seat.column, isSelected: seat.isSelected,
            line: seat.line, number: seat.number, price_discount: seat.price_discount, tariff: seat.tariff, woman_space: seat.woman_space
          });
        }
      });
    }
    console.log('sel', this.tripData.tripSeatsSelecteds);
    this.route.navigate(['/full-view']);
  }

  dateFormatter(date){
    if (!environment.production){
      console.log(date);
    }

    const dayOfMonth = date.substr(8,2);
    const month = date.substr(5,2);
    const year = date.substr(0,4);
    return `${dayOfMonth}/${month}/${year}`;
  }
}
