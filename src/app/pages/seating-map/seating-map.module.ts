import { Component, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { SeatingMapPageRoutingModule } from './seating-map-routing.module';

import { SeatingMapPage } from './seating-map.page';
import { ComponentesModule } from 'src/app/componentes/componentes.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    SeatingMapPageRoutingModule,
    ComponentesModule
  ],
  declarations: [SeatingMapPage]
})
export class SeatingMapPageModule {}
