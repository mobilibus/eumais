import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { IonicModule } from '@ionic/angular';
import { PgtoConcluidoPageRoutingModule } from './pgto-concluido-routing.module';
import { PgtoConcluidoPage } from './pgto-concluido.page';
import { ComponentesModule } from 'src/app/componentes/componentes.module';
import { QRCodeModule } from 'angularx-qrcode';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    PgtoConcluidoPageRoutingModule,
    ComponentesModule,
    QRCodeModule
  ],
  declarations: [PgtoConcluidoPage]
})
export class PgtoConcluidoPageModule {}
