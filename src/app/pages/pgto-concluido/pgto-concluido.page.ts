import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Animation, AnimationController } from '@ionic/angular';
import { LoadingProvider } from 'src/app/componentes/utils/loading.utils';
import { ApiEucatur } from 'src/app/services/api-euca.service';
import { CartService } from 'src/app/services/cart.service';
import { environment } from 'src/environments/environment';

@Component({
  selector: 'app-pgto-concluido',
  templateUrl: './pgto-concluido.page.html',
  styleUrls: ['./pgto-concluido.page.scss'],
})
export class PgtoConcluidoPage implements OnInit {
  saleId: number;
  saleType: string = 'Road';
  listSales: any = [];
  showQRCode: boolean = false;

  constructor(
    private animCtrl: AnimationController,
    private cart: CartService,
    private activatedRoute: ActivatedRoute,
    private apiEuca: ApiEucatur,
    private loading: LoadingProvider
  ) { }

  ngOnInit() {
    this.activatedRoute.params
    .subscribe(params =>{
      this.saleId = params.sale_id

      if (!environment.production){
        console.log(this.saleId);
      }

    });
    this.playAnim();
    this.cart.cleanCart();
    this.getSaleData();
  }

  getSaleData(){
    this.loading.showLoading();
    this.apiEuca.getData(`sales/find?id=${this.saleId}`)
      .then((res)=>{
        this.listSales = res;
        console.log(res);
        this.loading.closeLoading();
      })
  }

  playAnim(){
    let animation: Animation = this.animCtrl.create()
      .addElement(document.querySelector('#content'))
      .duration(1500)
      .fromTo('opacity', '0', '1');
  }

  dateFormatter(date, type = 'EUA'){
    if (!environment.production){
      console.log(date);
    }
    let dayOfMonth = date.substr(8,2);
    let month = date.substr(5,2);
    let year = date.substr(0,4);

    if (type == 'BRL'){
      return `${dayOfMonth}/${month}/${year}`;
    } else {
      return `${year}-${month}-${dayOfMonth}`;
    }
  }


}
