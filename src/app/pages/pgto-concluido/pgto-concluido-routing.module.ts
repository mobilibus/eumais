import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { PgtoConcluidoPage } from './pgto-concluido.page';

const routes: Routes = [
  {
    path: '',
    component: PgtoConcluidoPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class PgtoConcluidoPageRoutingModule {}
