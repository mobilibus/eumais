import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { IonItemSliding } from '@ionic/angular';
import { AlertProvider } from 'src/app/componentes/utils/alert.utils';
import { LoadingProvider } from 'src/app/componentes/utils/loading.utils';
import { ApiEucatur } from 'src/app/services/api-euca.service';
import { UtilsService } from 'src/app/services/utils.service';
import { environment } from 'src/environments/environment';

@Component({
  selector: 'app-cartoes',
  templateUrl: './cartoes.page.html',
  styleUrls: ['./cartoes.page.scss'],
})
export class CartoesPage implements OnInit {
  showNewCard: boolean = false;
  listCards: any = [];

  // campos
  listCountries: any = [];
  textCpfCnpj: string;
  boolActive: any = false;
  defaultAddress: any;
  useMyDefaultAddress: boolean = false;
  textAddress: string;
  textCity: string;
  textComplement: string;
  textCountry: string;
  textNeighborhood: string;
  textNumber: string;
  textUF: string;
  textZipCode: string;
  textBirthDate: string;
  textCardNumber: string;
  textCVV: string;
  textFlag: string;
  textName: string;
  textPhone: string;
  textValidity: string;
  creditFlag: string = 'selecione';
  realValidity: string;
  manualFlags: any[] = [
    { name: 'Visa', val: 'visa' },
    { name: 'MasterCard', val: 'mastercard'},
    { name: 'Amex', val: 'amex'},
    { name: 'HiperCard', val: 'hipercard'},
    { name: 'Aura', val: 'aura'},
    { name: 'DinersClub', val: 'diners'},
    { name: 'Elo', val: 'elo'},
    { name: 'Maestro', val: 'maestro'},
    { name: 'VisaElectron', val: 'visaelectron'},
  ];
  dateNow: any = new Date();



  constructor(
    private apiEumais: ApiEucatur,
    private loading: LoadingProvider,
    private utils: UtilsService,
    private alert: AlertProvider,
    private activatedRoute: ActivatedRoute,
    private route: Router
  ) { }

  ngOnInit() {
    this.activatedRoute.params
      .subscribe(params =>{
        if (params.showCad){
          this.showNewCard = true;
        }
      })
    this.getDefaultAddress();
    this.listarPaises();
    this.getAllCards();

  }

  toggleNewCard(){
    if (this.showNewCard){
      this.showNewCard = false;
    } else {
      this.showNewCard = true;
    }
  }

  getDefaultAddress(){
    this.apiEumais.getData('person/address')
      .then((res:any)=>{
        this.defaultAddress = res.filter(f => f.is_default == true);
        this.defaultAddress = this.defaultAddress[0];
        if (!environment.production){
          console.log(this.defaultAddress);
        }
      });
  }

  listarPaises(){
    this.utils.generalGetApi('https://servicodados.ibge.gov.br/api/v1/paises/')
      .then((res:any)=>{
        this.listCountries = res.map(obj => obj.nome.abreviado);
        console.log(this.listCountries);
      })
  }

  consultaCEP(){
    this.loading.showLoading();
    this.utils.generalGetApi(`https://viacep.com.br/ws/${this.textZipCode}/json`)
    .then((res:any)=>{
      this.textCity = res.localidade;
      this.textComplement = res.complemento;
      this.textAddress = res.logradouro;
      this.textNeighborhood = res.bairro;
      this.textUF = res.uf;
      this.textCountry = 'Brasil';
      setTimeout(() => {
        this.loading.closeLoading();
      }, 300);
      if (!environment.production){
        console.log(res);
      }
    });
  }

  excluirCard(id){
    this.alert.showAlert('Ei, atenção...', '', 'Você realmente deseja excluir este cartão?',
    [
      {
        text: 'Sim',
        handler:()=>{
          this.loading.showLoading();
          this.apiEumais.deleteData(`person/cards/${id}`)
            .then((res:any)=>{
              this.loading.closeLoading();

              if (!environment.production){
                console.log(res);
              }

              setTimeout(() => {
                this.alert.showAlert('Excluído', '', 'Cartão excluído com sucesso!', ['Ok']);
              }, 100);
              this.getAllCards();
            });
        }
      },
      'Não'
    ])
  }

  data(){
    let validityYear = this.textValidity.split('T')[0].substr(0,4);
    let validityMonth = this.textValidity.split('T')[0].substr(5,2);
    this.realValidity = validityMonth+'/'+validityYear;
  }

  getAllCards(){
    this.apiEumais.getData('person/cards')
      .then((res:any)=>{
        if(!environment.production){
          console.log(res);
        }
        this.listCards = res;
      })
      .catch((err:any)=>{
        if (!environment.production){
          console.log('CARD ERROR: ', err);
        }
        this.listCards = [];
      })
  }

  cadastrarCartao(){
    let address = {};
    if (this.useMyDefaultAddress){
      address = {
        address: this.defaultAddress.address,
        city: this.defaultAddress.city,
        complement: this.defaultAddress.complement,
        country: this.defaultAddress.country,
        is_default: this.defaultAddress.is_default,
        neighborhood: this.defaultAddress.neighborhood,
        number: this.defaultAddress.number.toString(),
        uf: this.defaultAddress.uf,
        zip_code: this.defaultAddress.zip_code
      }
    } else {
      address = {
        address: this.textAddress,
        city: this.textCity,
        complement: this.textComplement,
        country: this.textCountry,
        is_default: false,
        neighborhood: this.textNeighborhood,
        number: this.textNumber.toString(),
        uf: this.textUF,
        zip_code: this.textZipCode.toString()
      }
    }
      this.boolActive = this.boolActive ? true : false;
      let body = {
        cpf_cnpj: this.textCpfCnpj,
        active: this.boolActive,
        address: address,
        birth_date: this.textBirthDate,
        card_number: this.textCardNumber.replace(/\s/g, ''),
        cvv: this.textCVV,
        flag: this.creditFlag,
        name: this.textName,
        phone: this.textPhone,
        validity: this.realValidity
      }

      if(!environment.production){
        console.log(body);
      }

      this.loading.showLoading();

      this.apiEumais.postData('person/cards', body)
        .then((res:any)=>{
          if (!environment.production) console.log(res);

          setTimeout(() => {
            this.loading.closeLoading();
            this.showNewCard = false;
            this.activatedRoute.params
              .subscribe(params =>{
                if (params.showCad){
                  window.history.back();
                } else {
                  this.getAllCards();
                }
              })

          }, 300);

        })
  }

  autoFlag(){
    var cardnumber = this.textCardNumber.replace(/[^0-9]+/g, '');
    if (!environment.production) console.log(cardnumber);


        var cards = {
            visa      : /^4[0-9]{12}(?:[0-9]{3})/,
            mastercard : /^5[1-5][0-9]{14}/,
            diners    : /^3(?:0[0-5]|[68][0-9])[0-9]{11}/,
            amex      : /^3[47][0-9]{13}/,
            discover  : /^6(?:011|5[0-9]{2})[0-9]{12}/,
            hipercard  : /^(606282\d{10}(\d{3})?)|(3841\d{15})/,
            elo        : /^((((636368)|(438935)|(504175)|(451416)|(636297))\d{0,10})|((5067)|(4576)|(4011))\d{0,12})/,
            jcb        : /^(?:2131|1800|35\d{3})\d{11}/,
            aura      : /^(5078\d{2})(\d{2})(\d{11})$/
        };

        for (var flag in cards) {
            if(cards[flag].test(cardnumber)) {
                this.creditFlag = flag;
            }
        }
  }

  showDetails(item: IonItemSliding){
    item.open('end');
  }

}
