import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { CartoesPageRoutingModule } from './cartoes-routing.module';

import { CartoesPage } from './cartoes.page';
import { ComponentesModule } from 'src/app/componentes/componentes.module';
import { NgxMaskIonicModule } from 'ngx-mask-ionic';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    CartoesPageRoutingModule,
    ComponentesModule,
    NgxMaskIonicModule
  ],
  declarations: [CartoesPage]
})
export class CartoesPageModule {}
