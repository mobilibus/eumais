export const mock = {
  sale_id: 86600954,
  item_params: [
    {
      road: {
        selected_seat_key: "eyJhbGciOiJIUzM4NCIsInR5cCI6IkpXVCJ9.eyJjcmVhdGVkX2F0IjoiMjAyMi0wNy0yOCAyMDoyNDo1NSIsImlkIjowLCJpc191bmlmaWVkX21hcCI6ZmFsc2UsIml0ZW1zX2lkIjoiW3tcInJvYWRfdHJhdmVsX2lkXCI6MTk5MDc4NCxcImdhdGV3YXlfdHlwZVwiOlwic2dlXCIsXCJleHRlcm5hbF9zZWxlY3Rpb25cIjpcIjI0MzMyMjU4XCIsXCJzZWF0XCI6NDIsXCJjcmVhdGVkX2F0XCI6XCIyMDIyLTA3LTI4IDE5OjI0OjU1XCIsXCJ1cGRhdGVkX2F0XCI6XCJcIixcInRhcmlmZlwiOm51bGwsXCJwcmljZVwiOjB9XSIsInNlYXQiOjAsInRyYXZlbF9pZCI6NjM2MCwidHJhdmVsX2tleSI6ImV5SmhiR2NpT2lKSVV6TTROQ0lzSW5SNWNDSTZJa3BYVkNKOS5leUpoY25KcGRtRnNJam9pTWpBeU1pMHdPQzB5TXlBeE5EbzFNRG93TUNJc0ltTnlaV0YwWldSZllYUWlPaUl5TURJeUxUQTNMVEk0SURJd09qSTBPalF4SWl3aVpHVndZWEowZFhKbElqb2lNakF5TWkwd09DMHlNeUF4TXpveE1Eb3dNQ0lzSW1SbGMzUnBibmxmYzJWamRHbHZibUZzWDJsa0lqb3lNaklzSW1sa0lqbzJNell3TENKcGRHVnRjMTlwWkNJNklqRTVPVEEzT0RRaUxDSnZjbWxuYVc1ZmMyVmpkR2x2Ym1Gc1gybGtJam95TWpNc0luQmhjblJ1WlhKZlkyOWtaU0k2SWprNU9Ua2lMQ0p3Y21WZmNHRnBaQ0k2Wm1Gc2MyVXNJbk5sYkd4ZlkyaGhibTVsYkNJNklrVXRZMjl0YldWeVkyVWlMQ0owY21GMlpXeGZhR0YyWlY5eVpYTnZkWEpqWlNJNlptRnNjMlVzSW5SNWNHVmZkSEpoZG1Wc0lqb2ljMmRsSW4wLklSc0lNMDBpdWpvbHNvd2pfLUZqWmxMN3N6cE92c2xmVnZiN3I4eEVWcTd2SXZtcTEydFBuN3NraGdaeDZ6VnUiLCJ1c2VyX2lkIjozODI5fQ.Jc6qBf2griBAaUlSBtdBbynya3gGPdDPq6rOPYqtRzGiEOMbG_ahQroSdek4DV0Q",
        with_return: false,
        direction: 0,
        utilizer: {
          id: 0,
          reference_id: 0,
          utilizer_type: "",
          sale_item_gateway_id: 0,
          first_name: "CRISTIANO FARIAS",
          last_name: "MARTINS",
          document_type: "RG",
          document_number: "8352656",
          cpf: "000.059.310-96",
          phone: "51991237166",
          birth_date: "1983-10-02",
          gender: "m",
          locate_country_id: 1
        },
        item_id: 12482858,
        gateway_id: 0,
        localizer: "91841816",
        transaction: ""
      },
      air: null
    }
  ],
  continue_payments: [
    {
      use_three_ds: false,
      form_token: "",
      public_key_js: "",
      url_client_js: "",
      url_bank_authenticate: "",
      pix: {
        expiration_at: "10m",
        base64: "iVBORw0KGgoAAAANSUhEUgAAAPoAAAD6CAIAAAAHjs1qAAAGtElEQVR42u3dW47rNhBFUc9/0skIAjSi2kXKXufTuO0HtSjgqIjk84/Iz+RjCQR3EdxFcBfBXQR3EdxFcBfBXQR3EdxFcBfcRXAXwV0EdxHcRXAXuZz7J0j9Hf7rb//ynn/526nvNvVZm+t2sxPccccdd9xxxx133Ac+/o/sNj+3uGzF+xTb78k6nHKCO+6444477rjjjvsM07oy1tvsCd/6OxSVuq7shRPccccdd9xxxx133M9zn6qVTypXUR+nxmpFHZxaK9xxxx133HHHHXfcVdXk2FA0pqnHUnXdLMZSuOOOO+6444477rj/7hGx4jLUI6RT2+mG31XfknDHHXfccccdd9xx//+Xpzje5PX3vl44wd3ruOPuddxx9zru5zO1BKeOeW2OY4q1mlqHu1DhjjvuuOOOO+64/yb3qWM9U5fkyXert1Ndxepx0tR3q0dguOOOO+6444477ri3P6+mPPVZpyr11K2h2E7Ftky/A+6444477rjjjvuPcq/HPVPHyJ5U1VO1rx4JrfGKbg2444477rjjjjvuuO+NNurjWVP1qx61nKqDm7+rGMnhjjvuuOOOO+64436GTn2cq6itmyOt2+p48Vlroy7ccccdd9xxxx133O/6z14erET1kayp20FN8Pg4CXfccccdd9xxxx33q0dRm2OjmkjxWVN/Wx/de0FVxR133HHHHXfccf9K7nXVu6Fibh7/ekudPXXrwR133HHHHXfccce9rVx1DdqsVlP/vj7+dcNWf9mYCXfccccdd9xxx/3V3DdHQpv1d+r96+NZBcfNelqP83DHHXfccccdd9xx36O/Wa02t8SpcVVRozd/C+6444477rjjjjvubVXdrKf1aKaoevWYqf5dr3vMgDvuuOOOO+644/6j3ItLW7zPKXab36F4/82jY2tjONxxxx133HHHHfcf5V7U1mLkVI9I6rp5fDSzYKB+jIE77rjjjjvuuOOO+zypzTq1SaS4tPXWPVU90y2KO+6444477rjj/qPcp45zPeFSVN6pavWEe0F8sxrWozTccccdd9xxxx133GfqV31Eqa56m0fZ6hHVqYpZfzfccccdd9xxxx133OdHKnU9TWtNtA5TI6F6rU79+xdMVXHHHXfccccdd9y//szM1CV8I7v6dnDDFqqv70VPZnDHHXfccccdd9xfx70mWx/DKpZ48xhZMbLZvCU9+T6444477rjjjjvuuM9XwE+cYkQ1tcQ3HIM7tb2vHRHijjvuuOOOO+64/yj3qeWYuiRTf1tUwGI960cIN9TuS8dMuOOOO+6444477q/jXlS6U68XY53i0p46KlfcwuqxI+6444477rjjjjvue9ugXuJT9bTYNsUYqKi2m0fucMcdd9xxxx133HHfqyD1iKo+Clas5+b7148Ejhzdwx133HHHHXfcccf90c+oRzNTW/SGbXbDmGlzi+KOO+6444477rjjvldVi6NX9Ujoyb+vK3497tkcIdXbA3fccccdd9xxxx3380szddmKilZn81hYfS2e3MJwxx133HHHHXfccT8zAijq5hur6hTfqdtT8Uigvg3hjjvuuOOOO+64436mMhZ1ebNG1+Ohml09IjxS/XHHHXfccccdd9x/lPtUbaqPghVb4tSIpz4eN3ULK243h6sq7rjjjjvuuOOO+9dwL7bB1FKmR46WR071LaNY8xseaeCOO+6444477rjjflfNKkZXNZcbKmO9zU7dDnDHHXfccccdd9xxP1AvBsmeGnWdWsNTdbao+y8YM+GOO+6444477ri/7ojY1Kjitnp01VGnqMrXR9mK9cQdd9xxxx133HHHvV36up7W9ai4PPUYq96iU/W9RYI77rjjjjvuuOOO+6l6NHXZPpdlaksXR9OeXMdiTXDHHXfccccdd9xxP5/NOjU1Iqlp1ke4irpfVG3ccccdd9xxxx133GdGS6eOhRW1rOBVb6HN61I/HrhozIQ77rjjjjvuuOP+NdxvG0VtHnU6dUTs1Bqeqqq444477rjjjjvuuJ//3xnU26aoZcXn1kfK6t9+6nEF7rjjjjvuuOOOO+7nuRfLcaQeLVf2eqsUTtauEe6444477rjjjjvueVWdGsE82ULF+9Tbux73FLew4jEG7rjjjjvuuOOOO+5746Enl23q8kx91ua23ByZ3WAAd9xxxx133HHHHfczI496tFFTniJS176ivm+O3nDHHXfccccdd9xxF/nC4C64i+AugrsI7iK4i+AugrsI7iK4i+AugrvgLoK7CO4iuIvgLoK7yE7+Be2KwPH+6qnkAAAAAElFTkSuQmCC",
        copyPaste: "00020126890014BR.GOV.BCB.PIX2567api-pix.bancobs2.com.br/spi/v2/515deafe-703b-46a2-9059-84fd54344e4d520400005303986540510.945802BR5925PAYMEE BRASIL SERVICOS DE6014Belo Horizonte61083038040362070503***63049B34",
        steps: {
          tips_qrcode: [
            "Acesse o APP ou o Internet Banking do seu Banco",
            "Acesse o menu PIX",
            "Abra o leitor de QRCode no APP e escaneie o código informado",
            "Ou copie o código e cole na função 'Copia e Cola'",
            "Siga os passos no APP e finalize a transação"
          ],
          tips_keys: null
        }
      }
    }
  ],
  error: ""
}
