import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { AlertProvider } from 'src/app/componentes/utils/alert.utils';
import { LoadingPaymentProvider } from 'src/app/componentes/utils/loading-payment.utils';
import { ApiEucatur } from 'src/app/services/api-euca.service';
import { CartService } from 'src/app/services/cart.service';
import { DeviceInfoService } from 'src/app/services/device-info.service';
import { UserService } from 'src/app/services/user.service';
import { environment } from 'src/environments/environment';
import { Clipboard } from '@ionic-native/clipboard/ngx';

@Component({
  selector: 'app-pgto-um',
  templateUrl: './pgto-um.page.html',
  styleUrls: ['./pgto-um.page.scss'],
})
export class PgtoUmPage implements OnInit {
  listCards: any = [];
  subtotal: any = 0;
  selectedCard: any = false;
  textCVV: string;
  textRG: string;
  textPhone: string;
  textPayType: string = "credit";
  defaultInstallments = 1;
  isPIXmethod = false;
  maxParcelas = 10;
  arrParcelas: any[] = [];
  disablePgtoBtn = false;
  showPixData = false;
  pixData:any;
  showHint = false;

  constructor(
    private apiEumais: ApiEucatur,
    private activatedRoute: ActivatedRoute,
    private alert: AlertProvider,
    private route: Router,
    private user: UserService,
    private cart: CartService,
    private loadingPaymento: LoadingPaymentProvider,
    private deviceInfo: DeviceInfoService,
    private clipboard: Clipboard
  ) { }

  ngOnInit() {
    this.activatedRoute.params
    .subscribe(params =>{
      this.subtotal = params.subtotal

      if (!environment.production){
        console.log(this.subtotal);
      }
    });

    this.maxParcAvailable();
  }

  ionViewWillEnter(){
    this.getAllCards();
  }

  getAllCards(){
    this.apiEumais.getData('person/cards')
      .then((res:any)=>{
        if(!environment.production){
          console.log(res);
        }
        this.listCards = res;
      })
      .catch((err:any)=>{
        if (!environment.production){
          console.log('CARD ERROR: ', err);
        }
        this.listCards = [];
      })
  }

  selectCard(card){
    if (card.isSelected){
      this.isPIXmethod = false;
      this.listCards.map(obj=>{
        if (obj.card_id !== card.card_id && obj.isSelected){
          obj.isSelected = false;
        }
      });
      this.selectedCard = card;
      console.log(this.selectedCard);
    }
  }

  efetuarPgto(){
    this.disablePgtoBtn = true;
    if (!this.isPIXmethod){
      if (!this.selectedCard){
        this.alert.showAlert('Oops', '', `Você não selecionou um cartão para efetuar o pagamento.`, ['Ok']);
      } else if (this.textCVV == '' || this.textCVV == undefined){
        this.alert.showAlert('Oops', '', `Você não informou o CVV do cartão selecionado.`, ['Ok']);
      } else if (this.textPayType == '' || this.textPayType == undefined){
        this.alert.showAlert('Opa!', '', `Sei da sua ansiedade para viajar, por isso selecione a forma de pagamento para dar continuidade a compra!`, ['Ok']);
      } else {
        let body = {
          email: this.user.userData.email,
          items: this.cart.items,
          payments: [
            {
              amount: parseFloat(this.subtotal),
              card: {
                id: this.selectedCard.card_id,
                cvv: this.textCVV,
              },
              method: `${this.textPayType}_card`,
              // redirect_to: 'https://c345-191-253-125-30.sa.ngrok.io/events/health-check'
            }
          ]
        }
        if (this.textPayType == "credit"){
          body.payments[0].card["installments"] = parseInt(this.defaultInstallments.toString());
        }
        this.callSaleEndpoint(body);
      }
    } else {
      let body = {
        email: this.user.userData.email,
        items: this.cart.items,
        payments: [
          {
            amount: parseFloat(this.subtotal),
            method: "pix",
            // redirect_to: 'https://c345-191-253-125-30.sa.ngrok.io/events/health-check',
            // redirect_to_refused: 'https://c345-191-253-125-30.sa.ngrok.io/events/health-check',
          }
        ]
      }
      this.callSaleEndpoint(body);
    }
  }

  callSaleEndpoint(body:any) {
    this.loadingPaymento.showLoading();
    // chama a api de pagamento
    this.apiEumais.postData(`sales?device_type=app&device_subtype=${this.deviceInfo.phoneInfo.platform}`, body)
    .then((res)=>{
      console.log(res);

      this.loadingPaymento.closeLoading();
      if (!this.isPIXmethod){
        if (res['error'] == '' && res['sale_id'] > 0){
          this.route.navigate([`/ver-compra/${res['sale_id']}/1`]);
        }
      } else {
        this.showPixData = true;
        this.pixData = res;

        // chama a rota de venda para recuperar se o pagamento foi efetuado
        setInterval(()=>{
          this.callSaleData();
        }, 25000)
      }

    })
    .catch((err)=>{
      this.disablePgtoBtn = false;
      console.log(err);
      this.loadingPaymento.closeLoading();
      setTimeout(() => {
        this.alert.showAlert(
          `Oopa!`,
          '',
          `<img src="./assets/imgs/alert_icon.png"><br>
          Parece que tivemos um problema com seu pagamento, "${err.error.message}" quer tentar outra vez?`,
          [
            { text: 'Sim' },
            {
              text: 'Não',
              handler: ()=>{
                this.cart.cleanCart();
                this.route.navigate(['/home']);
              }
            }
          ]
        );
      }, 100);
    });
  }

  pixSelected(){
    if (this.isPIXmethod){
      this.listCards.map(obj=>obj.isSelected = false);
      this.selectedCard = false;
    }
  }

  maxParcAvailable() {
    let arrParcelas = [];
    for (let i = this.maxParcelas; i !== 0; i-- ){
        if ((parseFloat(this.subtotal) / i) >= 10){
          let parcela = i;
          let valorParcela = parseFloat(this.subtotal) / i;
          arrParcelas.push({ parcela, valorParcela });
        }
    }
    this.arrParcelas = arrParcelas;
    console.log(this.arrParcelas);
  }

  copy(pixPayment){
    this.clipboard.copy(pixPayment.copyPaste);
    pixPayment.copied = true;
    setTimeout(() => {
      this.showHint = true;
    }, 8000);
  }

  callSaleData(){
    this.apiEumais.getData(`sales/find?id=${this.pixData.sale_id}`)
      .then((res)=>{
        let result = res[0];
        if (result["payments"][0].statuses[0].status == "Confirmado"){
          this.route.navigate([`/ver-compra/${this.pixData.sale_id}/1`]);
        } else {
          console.log("PIX Status: ", result["payments"][0].statuses[0]);
        }
      })
  }
}
