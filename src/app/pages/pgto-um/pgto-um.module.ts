import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { PgtoUmPageRoutingModule } from './pgto-um-routing.module';

import { PgtoUmPage } from './pgto-um.page';
import { ComponentesModule } from 'src/app/componentes/componentes.module';
import { QRCodeModule } from 'angularx-qrcode';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    PgtoUmPageRoutingModule,
    ComponentesModule,
  ],
  declarations: [PgtoUmPage]
})
export class PgtoUmPageModule {}
