import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { PgtoUmPage } from './pgto-um.page';

const routes: Routes = [
  {
    path: '',
    component: PgtoUmPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class PgtoUmPageRoutingModule {}
