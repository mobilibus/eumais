import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { LoadingProvider } from 'src/app/componentes/utils/loading.utils';
import { ApiEucatur } from 'src/app/services/api-euca.service';
import { environment } from 'src/environments/environment';

@Component({
  selector: 'app-compras',
  templateUrl: './compras.page.html',
  styleUrls: ['./compras.page.scss'],
})
export class ComprasPage implements OnInit {
  listSales: any = [];

  constructor(
    private apiEuca: ApiEucatur,
    private loading: LoadingProvider,
    private route: Router
  ) { }

  ngOnInit() {
    this.getSalesFromUserId();
  }

  getSalesFromUserId(){
    this.loading.showLoading();
    this.apiEuca.getData('sales/find?sale_type=Road')
      .then((response)=>{
        let res: any = response;
        // Coloca a última acima e assim por diante
        this.listSales = res.sort((a,b)=> b.operation_date - a.operation_date ? 1 : -1);
        if (!environment.production) console.log(this.listSales);
        this.loading.closeLoading();
      });
  }

  goToBuy(id){
    if (!environment.production) console.log(id);

    this.route.navigate([`/ver-compra/${id}`]);
  }

  dateFormatter(date, type = 'EUA'){
    if (!environment.production) console.log(date);

    let dayOfMonth = date.substr(8,2);
    let month = date.substr(5,2);
    let year = date.substr(0,4);

    if (type == 'BRL'){
      return `${dayOfMonth}/${month}/${year}`;
    } else {
      return `${year}-${month}-${dayOfMonth}`;
    }
  }

}
