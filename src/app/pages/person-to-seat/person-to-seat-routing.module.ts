import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { PersonToSeatPage } from './person-to-seat.page';

const routes: Routes = [
  {
    path: '',
    component: PersonToSeatPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class PersonToSeatPageRoutingModule {}
