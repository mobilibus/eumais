import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { PersonToSeatPageRoutingModule } from './person-to-seat-routing.module';

import { PersonToSeatPage } from './person-to-seat.page';
import { ComponentesModule } from 'src/app/componentes/componentes.module';
import { NgxMaskIonicModule } from 'ngx-mask-ionic';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    PersonToSeatPageRoutingModule,
    ComponentesModule,
    NgxMaskIonicModule
  ],
  declarations: [PersonToSeatPage]
})
export class PersonToSeatPageModule {}
