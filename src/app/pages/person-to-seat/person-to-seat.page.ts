/* eslint-disable @typescript-eslint/naming-convention */
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { AlertProvider } from 'src/app/componentes/utils/alert.utils';
import { TripDataService } from 'src/app/services/trip-data.service';
import { UserService } from 'src/app/services/user.service';
import { UtilsService } from 'src/app/services/utils.service';
import { environment } from 'src/environments/environment';

@Component({
  selector: 'app-person-to-seat',
  templateUrl: './person-to-seat.page.html',
  styleUrls: ['./person-to-seat.page.scss'],
})
export class PersonToSeatPage implements OnInit {
  // Variáveis
  maxYear: number;
  docMask: string;
  seatNumber: number;
  seatType: string;
  seatOwnerItsMe = false;
  seatOwnerBirth: string;
  seatOwnerCPF: string;
  seatOwnerRGNumber: string;
  seatOwnerDocumentType: string;
  seatOwnerFirstName: string;
  seatOwnerGender: string;
  seatOwnerLastName: string;
  seatOwnerLocateCountryId: any;
  seatOwnerPhone: string;


  constructor(
    private activatedRoute: ActivatedRoute,
    private tripData: TripDataService,
    private alert: AlertProvider,
    private route: Router,
    private user: UserService,
    private utils: UtilsService
  ) { }

  ngOnInit() {
    const now = new Date();
    this.maxYear = now.getFullYear();
    this.activatedRoute.params
    .subscribe(params =>{
      this.seatNumber = params.number;
      this.seatType = params.type;

      if (!environment.production){
        console.log('Number: ', this.seatNumber);
        console.log('Type: ', this.seatType);
      }

    });
  }

  ionViewWillEnter(){
    // Pega os dados já cadastrados (se existirem) no seat
    const seat = this.tripData.tripSeatsSelecteds.map(obj=>obj).filter(s=>s.number == this.seatNumber);
    if (seat[0].utilizer){
      this.seatOwnerFirstName = seat[0].utilizer.first_name;
      this.seatOwnerLastName = seat[0].utilizer.last_name;
      this.seatOwnerBirth = seat[0].utilizer.birth;
      this.seatOwnerLocateCountryId = 1;
      this.seatOwnerPhone = seat[0].utilizer.phone;
      this.seatOwnerDocumentType = seat[0].utilizer.document_type;
      this.seatOwnerCPF = seat[0].utilizer.cpf;
      this.seatOwnerRGNumber = seat[0].utilizer.document_number;
      this.seatOwnerGender = seat[0].utilizer.gender;
      setTimeout(() => {
        if (seat[0].utilizer.cpf == this.user.userData.cpf){
          this.seatOwnerItsMe = true;
        }
      }, 30);
    }
  }

  mySeatClick(){
    setTimeout(() => {
      if (this.seatOwnerItsMe){
        this.seatOwnerFirstName = this.user.userData.firstName;
        this.seatOwnerLastName = this.user.userData.family_name;
        this.seatOwnerBirth = this.user.userData.birth_date;
        this.seatOwnerLocateCountryId = '+55';
        this.seatOwnerPhone = this.user.userData.phone;
        this.seatOwnerDocumentType = 'rg';
        this.seatOwnerCPF = this.user.userData.cpf;
        this.seatOwnerRGNumber = this.user.userData.rg;
        this.seatOwnerGender = this.user.userData.gender;
      } else {
        this.seatOwnerFirstName = '';
        this.seatOwnerLastName = '';
        this.seatOwnerBirth = '';
        this.seatOwnerLocateCountryId = '';
        this.seatOwnerPhone = '';
        this.seatOwnerDocumentType = '';
        this.seatOwnerCPF = '';
        this.seatOwnerRGNumber = '';
        this.seatOwnerGender = '';
      }
    }, 100);

  }

  dateBirth(e){
    this.seatOwnerBirth = this.dateFormatter(e.detail.value);

    if (!environment.production){
      console.log(this.seatOwnerBirth);
    }
  }

  dateFormatter(date){
    if (!environment.production){
      console.log(date);
    }

    const dayOfMonth = date.substr(8,2);
    const month = date.substr(5,2);
    const year = date.substr(0,4);
    return `${year}-${month}-${dayOfMonth}`;
  }

  womanTest(){
    const index = this.tripData.tripSeatsSelecteds.findIndex( i => i.number == this.seatNumber);
    console.log(this.seatOwnerGender);
    if (this.tripData.tripSeatsSelecteds[index].woman_space && this.seatOwnerGender == 'Masculino'){
      this.alert.showAlert(
        'Atenção', '', ` A poltrona que você selecionou é reservada ao Espaço Mulher. Pedimos que selecione outra poltrona.`, ['Ok']);
      this.seatOwnerGender = '';
    }
  }

  concluir(){
    if (this.seatOwnerFirstName == undefined || this.seatOwnerFirstName == ''){
      this.alert.showAlert('Oops...', '', 'Você precisa informar o primeiro nome do passageiro.', ['Ok']);
    } else if (this.seatOwnerLastName == undefined || this.seatOwnerLastName == ''){
      this.alert.showAlert('Oops...', '', 'Você precisa informar o sobrenome do passageiro.', ['Ok']);
    } else if (this.seatOwnerCPF == undefined || this.seatOwnerCPF == ''){
      this.alert.showAlert('Oops...', '', 'Você precisa informar o CPF do passageiro.', ['Ok']);
    } else if (this.seatOwnerBirth == undefined || this.seatOwnerBirth == ''){
      this.alert.showAlert('Oops...', '', 'Você precisa informar a data de nascimento do passageiro.', ['Ok']);
    } else if (this.seatOwnerDocumentType == undefined || this.seatOwnerDocumentType == ''){
      this.alert.showAlert('Oops...', '', 'Você precisa informar o tipo de documento do passageiro.', ['Ok']);
    } else if (this.seatOwnerRGNumber == undefined || this.seatOwnerRGNumber == ''){
      this.alert.showAlert('Oops...', '', 'Você precisa informar o número do documento do passageiro.', ['Ok']);
    } else if (this.seatOwnerLocateCountryId == undefined || this.seatOwnerLocateCountryId == ''){
      this.alert.showAlert('Oops...', '', 'Você precisa informar o DDI do telefone do passageiro.', ['Ok']);
    } else if (this.seatOwnerPhone == undefined || this.seatOwnerPhone == ''){
      this.alert.showAlert('Oops...', '', 'Você precisa informar o número de telefone do passageiro.', ['Ok']);
    } else {
      const index = this.tripData.tripSeatsSelecteds.findIndex( i => i.number == this.seatNumber);
      this.tripData.tripSeatsSelecteds[index].utilizer = {
        first_name: this.seatOwnerFirstName,
        last_name: this.seatOwnerLastName,
        birth: this.seatOwnerBirth,
        cpf: this.seatOwnerCPF,
        document_type:this.seatOwnerDocumentType.toUpperCase(),
        document_number: this.seatOwnerRGNumber,
        gender: this.seatOwnerGender == 'Masculino' ? 'm' : 'f',
        locate_country_id: 1,
        phone: this.utils.apenasNumeros(this.seatOwnerPhone).toString()
      };
      this.route.navigate(['/full-view']);
    }


  }





}
