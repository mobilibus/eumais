import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { PgtoRecusadoPage } from './pgto-recusado.page';

const routes: Routes = [
  {
    path: '',
    component: PgtoRecusadoPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class PgtoRecusadoPageRoutingModule {}
