import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AlertProvider } from 'src/app/componentes/utils/alert.utils';
import { LoadingProvider } from 'src/app/componentes/utils/loading.utils';
import { ToastProvider } from 'src/app/componentes/utils/toast.utils';
import { ApiEucatur } from 'src/app/services/api-euca.service';
import { CartService } from 'src/app/services/cart.service';
import { TripDataService } from 'src/app/services/trip-data.service';
import { environment } from 'src/environments/environment';

@Component({
  selector: 'app-full-view',
  templateUrl: './full-view.page.html',
  styleUrls: ['./full-view.page.scss'],
})
export class FullViewPage implements OnInit {
  // Variáveis
  allSeatsOkay: boolean;

  constructor(
    public tripData: TripDataService,
    private route: Router,
    private alert: AlertProvider,
    private apiEuca: ApiEucatur,
    private loading: LoadingProvider,
    public cart: CartService,
    private toast: ToastProvider,
  ) { }

  ngOnInit() {
    if (!environment.production) console.log(this.tripData);
  }

  ionViewWillEnter(){
    const seat = this.tripData.tripSeatsSelecteds.map(obj=>obj).filter(s=>s.utilizer);
    if (seat.length === this.tripData.tripSeatsSelecteds.length){
      this.allSeatsOkay = true;
    }
  }

  dateTreat(date: string, type: string = 'BRL'){
    const year = date.substring(0, 4);
    const month = date.substring(5, 7);
    const days = date.substring(8,10);
    return `${days}/${month}/${year}`;
  }

  selectPassenger(seat){
    this.route.navigate([`/person-to-seat/${seat.number}/${seat.category}`]);
  }

  esqPlan(){
    this.alert.showAlert(
      'Tem certeza?',
      '',
      'Não será possível desfazer esta ação. Precisando iniciar o processo novamente.',
      [
        {
          text: 'Sim',
          handler: ()=>{
            this.tripData.cleanData();
            setTimeout(() => {
              this.route.navigate(['/home']);
            }, 30);
          }
        },
        'Não'
      ]
    );
  }

  reservarAssentos(){
    this.alert.showAlert('Reservar?',
    '',
    `Deseja reservar ${this.tripData.tripSeatsSelecteds.length > 1 ? 'os assentos' : 'o assento'}?`,
    [
      {
        text: 'Sim',
        handler: ()=>{
          this.loading.showLoading();
          let count = 0;
          const seats = [];
          // Percorre os assentos selecionados
          this.tripData.tripSeatsSelecteds.map((obj: any)=>{
            // seta o body com o número do seat selecionado e o hash da viagem
            const body = {
              items: [
                { seat: obj.number }
              ],
              // eslint-disable-next-line @typescript-eslint/naming-convention
              travel_key: this.tripData.tripHash
            };
            if (!environment.production) console.log(body);
            // chama a api para enviar o body acima e receber a selected_seat_key
            this.apiEuca.postData(`road/travels/${this.tripData.tripHash}/seats`, body)
              .then((res: any)=>{
                count += 1;
                const seat = this.tripData.tripSeatsSelecteds.filter(f=> f.number === obj.number);
                seat[0].selected_seat_key = res.selected_seat_Key;

                // adiciona à array de seats
                seats.push(seat[0]);

                if (!environment.production){
                  console.log(res);
                }
                // adiciona ao carrinho
                this.cart.addTripToCart(seats);


                // testa se todos os assentos foram percorridos e processados pela api
                if (count === this.tripData.tripSeatsSelecteds.length){
                  setTimeout(() => {
                    this.loading.closeLoading();
                    this.tripData.tripIsReserved = true;
                    // eslint-disable-next-line max-len
                    this.toast.showToast(`${this.tripData.tripSeatsSelecteds.lenth > 1 ? 'Assentos reservados com sucesso.' : 'Assento reservado com sucesso.'}`);
                    setTimeout(() => {
                      // limpa os dados da trip para estar pronta para outra
                      this.route.navigate(['/carrinho']);
                    }, 1000);
                  }, 300);
                }
              })
              .catch((err: any)=>{
                this.loading.closeLoading();
                if (!environment.production){
                  console.log(err);
                }
                this.alert.showAlert('Oops', 'Ocorreu um erro...', err.error.message,
                [
                  {
                    text: 'Selecionarei outra',
                    handler: ()=>{
                      this.tripData.cleanData();
                      setTimeout(() => {
                        window.history.back();
                      }, 300);
                    }
                 }
                ]);

              });
          });

        }
      },
      'Não'
    ]
    );
  }




}
