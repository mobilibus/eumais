import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { FullViewPage } from './full-view.page';

const routes: Routes = [
  {
    path: '',
    component: FullViewPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class FullViewPageRoutingModule {}
