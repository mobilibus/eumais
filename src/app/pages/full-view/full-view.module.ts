import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { FullViewPageRoutingModule } from './full-view-routing.module';

import { FullViewPage } from './full-view.page';
import { ComponentesModule } from 'src/app/componentes/componentes.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    FullViewPageRoutingModule,
    ComponentesModule,
  ],
  declarations: [FullViewPage]
})
export class FullViewPageModule {}
