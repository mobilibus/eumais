import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { AlertProvider } from 'src/app/componentes/utils/alert.utils';
import { LoadingProvider } from 'src/app/componentes/utils/loading.utils';
import { ApiEucatur } from 'src/app/services/api-euca.service';
import { CartService } from 'src/app/services/cart.service';
import { UserService } from 'src/app/services/user.service';
import { verCompraMock } from './ver-compra.mock';

@Component({
  selector: 'app-ver-compra',
  templateUrl: './ver-compra.page.html',
  styleUrls: ['./ver-compra.page.scss'],
})
export class VerCompraPage implements OnInit {
  saleId: number = 0;
  origin = false;
  selectedSale: any;
  showQRCode = false;

  constructor(
    private loading: LoadingProvider,
    private route: Router,
    private activatedRoute: ActivatedRoute,
    private apiEuca: ApiEucatur,
    private alert: AlertProvider,
    private user: UserService,
    private cart: CartService,
  ) { }

  ngOnInit() {
    this.loading.showLoading();
    this.activatedRoute.params
      .subscribe(params =>{
        if (params.saleId){
          this.saleId = params.saleId;
        }
        if (params.origin){
          this.origin = true
          this.cart.cleanCart();
        }
      });

    if (this.saleId.toString() == "0101"){
      this.selectedSale = verCompraMock;
      setTimeout(() => {
        this.loading.closeLoading();
      }, 3500);
    } else {
      this.getSaleInfo();
    }
  }

  getSaleInfo(){
    this.apiEuca.getData(`sales/find?id=${this.saleId}`)
      .then((res)=>{
        this.selectedSale = res[0];
        if (this.selectedSale !== undefined){
          console.log(this.selectedSale);
          setTimeout(() => {
            this.loading.closeLoading();
          }, 1000);
        }
      });
  }

  goToCheckout(){
    this.route.navigate([`/pgto-um/${this.selectedSale.amount}/${this.saleId}`]);
  }

  compareDate(tripDate, tipo?){
    let date = new Date(tripDate.substr(0,10));
    let today = new Date();
    date.setDate(date.getDate() + 1);
    let timeDiff = today.getTime() - date.getTime();
    let passTripDay = date.getTime() < today.getTime();
    let daysDiff = Math.ceil(timeDiff / (1000 * 3600 * 24));
    console.log(daysDiff, passTripDay, tipo);
    return { daysDiff, passTripDay };
  }

  cancelTrip(itemKey: string){
    this.alert.showAlert("Tem certeza?", "", "Você realmente deseja cancelar esta viagem?",
    [
      {
      text: "Sim",
      handler: ()=>{
        this.loading.showLoading();
        console.log(itemKey);
        this.apiEuca.getData(`sales/cancel_conditions/${itemKey}`)
          .then((res)=>{
            console.log(res);
            if (res["status"] == "authorized"){
              this.apiEuca.postData(`sales/cancel_item/${res["key"]}/Cancel`, {})
                .then((response)=>{
                  console.log(response);
                  this.loading.closeLoading();
                  this.alert.showAlert("Concluído", "", "Transação cancelada com sucesso. Dentro de alguns instantes, o valor será estornado na conta bancária pela qual você realizou a compra, ou se foi realizada com cartão de crédito, o estorno será realizado em sua fatura, seguindo as regras de sua administradora.", ["Ok"]);
                  setTimeout(() => {
                    this.route.navigate["/compras"];
                  }, 1000);
                })
                .catch((err)=>{
                  this.alert.showAlert("Oops", "", "Não foi possível cancelar. Entre em contato com o suporte.", ["Ok"]);
                })
            }
          });
      }
    },
    {
      text: "Não"
    }
    ]);
  }


  monitorarViagem(code){
    this.route.navigate([`/monitorar-viagem/${code}`]);
  }

  showBpe(sale, item){
    this.alert.showAlert("Tem certeza?", "", `Após emitir o bilhete eletrônico, qualquer alteração ou devolução só podera ocorrer em uma agência física.`,
    [
      {
        text: "Gerar Bilhete",
        handler: () => {
          console.log(sale, item);
          this.apiEuca.postData(`sales/${sale.sale_id}/items/${item.key}/request_bpe`, {})
            .then((res)=>{
              if (res){
                this.alert.showAlert("Sucesso", "", `Bilhete eletrônico solicitado com sucesso. Em breve será possível vê-lo em sua caixa de entrada de ${this.user.userData.email}`, ["Ok"])
              }
            });
        }
      },
      {
        text: "Não"
      }
    ])
  }
}
