export const verCompraMock = {
    id: 86687990,
    sectional_id: 2280,
    sectional_cod: "9999",
    sectional_name: "AGÊNCIA WEB",
    person_id: 0,
    person_client_id: 4887146,
    person_salesman_id: 257251,
    operation_date: "2022-08-03",
    type: "Venda",
    system: "app_eumais",
    confirmed: true,
    created_at: "2022-08-03 18:05:59",
    amount: 162.19,
    items: [
      {
        key: "VIB-12583626",
        sale_id: 86687990,
        localizer: "00019-VOUCHER-5F828-0999949757",
        transaction: "",
        created_at: "2022-08-03 18:05:59",
        bpe: {
          id: 0,
          contingency: "",
          access_key: "",
          protocol: "",
          authorization: "",
          public_url: "",
          qrcode: ""
        },
        nfse: {
          id: 0
        },
        road: {
          travel_item_id: 0,
          ticket_item_id: 91946682,
          travel_item_service_travel: "",
          datetime_start: "2022-08-04 09:20:00",
          datetime_departure: "2022-08-04 09:20:00",
          datetime_arrival: "2022-08-04 21:45:00",
          duration: "745",
          eticket_number: "",
          seat_number: 46,
          direction: "2",
          status: "confirmed",
          company_issued: {
            acronym_state: ""
          },
          boarding: {
            id: 1020,
            code: "6451",
            name: "SÃO PAULO - TIETÊ",
            locality: {
              name: "SÃO PAULO",
              state: {
                acronym: "SP"
              }
            }
          },
          landing: {
            id: 82,
            code: "0241",
            name: "FLORIANÓPOLIS",
            locality: {
              name: "FLORIANÓPOLIS",
              state: {
                acronym: "SC"
              }
            }
          },
          company: {
            usual_name: "SOLIMOES",
            logo: "https://res.cloudinary.com/eumais/image/upload/v1574099142/logo/companies/eucatur.png"
          },
          line: {
            code: "15431",
            prefix: "16-0154-31",
            description: "FLORIANÓPOLIS x SÃO PAULO - TIETÊ",
            federal_code: "",
            federal_description: "FLORIANÓPOLIS x SÃO PAULO - TIETÊ"
          },
          tribute: {
            icms_percentage: 12,
            icms_value: 17.97,
            other_taxes_percentage: 25.45,
            other_taxes_value: 61.81,
            intermediation_tax: 37.43,
            intermediation_percentage: 25
          },
          centralizing_company: {
            name: "SOLIMOES TRANSPORTES DE PASSAGEIROS E CARGAS EIRELI",
            cnpj: "07.549.414/0033-09",
            state_registration: "144737905112",
            municipal_registration: "1233456",
            address: {
              public_place: "CENTRO",
              number: "123",
              neighborhood: "CENTRO",
              cep: "",
              complement: "EUCATUR",
              locality: {
                name: "SÃO PAULO",
                ibge_code: "3550308",
                state: {
                  acronym: "SP",
                  icms_percentage: "12"
                }
              }
            }
          },
          vehicle: {
            bpe_code: "3",
            bpe_description: "Semileito"
          },
          electronic_boarding: {
            qrcode_bpe: "iVBORw0KGgoAAAANSUhEUgAAAQAAAAEAAQMAAABmvDolAAAABlBMVEX///8AAABVwtN+AAADx0lEQVR42uyZMY70qhKFD3JAWDvAG7GabTn4JSxN4G1hsRHYASGBxXkqevrN/cMbNLeDIbJaX1ANVadOAX7X7/qXK5B9JaWG89gAkwXUL09yFrADD9OlsoUDm8MKNgTGByCfA/j7gbVWBH4lspBieSzxsfapgH7LeX7dMH1FZbP3NhtwphK7fQZZgT3cmAoAD+cgCAeWaDoq7YENf53mmwHNaudEdKNSKiXX2vZ92/5K+/8a0OUcqpyaR9+nie1vCXgzsGPzWv88eftxmtLCmUhmmQVAZcdULa0vxpWZsC3cPrqVswCbVAcr6x4YtzUDovHqTsksYF/SxVJZLfVnQ4KHjctV8ucANsJnCAHLdK8ZlWyW6SqlzgJ2wDCDUAFO3a0UlSB/MWMaoGXcRYRtSXGIkVaWj+a1kzOAdGUHSlMB1qwmTw13ffXuCcASH6ZDquXhb6yjsuBTNK+N+gAgpHtUWR09qzBXNHvevsPJLADbwtIBOdS5mEJU7IE3XkFOAGz00UFqtccS1T9Q2Ha1fYWzgNEBu36Ew8e1a26Fw6drOIlJADDypQJ/fOpaT/Y8o+//8DCfADxUd7TgMEpPbAspRRjOAoBNZRBVu6UeIbW0Fl7dYRYQtG86VJ7noYqYK55WYiKw+xtqeiuwpGhKBtqO5YZzMguwcXusrCItkKmMXA5NJ4L8OUCImxpOyNng9V+MNp5UlKcBgFejJdj3Pws1t2h56EQ7Dwi8TSmkStDCbChQA5yu7qYB37OGAOFWO6MdW4/tx+xNADSXs0BVDptzYLWnTs3m5eU+ALC8vW6k/qyzSq6jX/Aqr775fkBlUAFRv5n0NEeNUd34LGDH8A989k2HVZXYMj5++sXbgUBepYt6mCXda0dFCzq75nUasPtbBxBtB/e2ZJPrmF4ZTcfHACExOqfap0owuvhIs8dcgKXrgLKr9ySlYoePD/O6A3k/YMeWyBjPNox5vsGSV17rLGBf2HWgJw/tTisrW2g+XXyd5vuBQHVtdQS0bSPe9myLhZ8DJHZTqsC2P9uS1yxizzOln775fsA+5yGq2VviqpPsmBt9/jZ7EwAs0VPNkz1UgVWVNaHUhco0AGr5ayXHJWrJkLbvy43XADIBGK8PGbXa8+knx53u9y33pwDj9SGLduyvFE1fhTrab1h/7qPeDmg8rJBmD/98fYDGS84FNK2l2Ru+m0zYw3IYv5kAHDSqY0mF+XmJqm58HgCMlyChtmYW9VH2ZGIpnwN8ZzXlPMK9MENRzShn6izgd/2u/6//BQAA//+yyCbTaFQ6wAAAAABJRU5ErkJggg==",
            TicketGate: {
              type: "",
              number: "",
              type_tariff: "",
              image: ""
            }
          }
        },
        travel_insurance: {
          ticket_id: 0,
          certified_id: 0,
          certified_number: "",
          certified_series: "",
          certified_company: "",
          reference_value: 0,
          reference_km: 0,
          sales_channel: ""
        },
        amount: 162.19,
        utilizers: [
          {
            first_name: "GUILHERME",
            last_name: "AUGUSTO CUQUI",
            document_type: "RG",
            document_number: "5377612",
            birth: "",
            locate_country_id: 0
          }
        ],
        components: [
          {
            id: 313593,
            sale_item_type: "VendaItemBPe",
            sale_item_id: 12583626,
            type: "toll",
            value: 4.98
          },
          {
            id: 313594,
            sale_item_type: "VendaItemBPe",
            sale_item_id: 12583626,
            type: "intermediation_tax",
            value: 37.43
          },
          {
            id: 313591,
            sale_item_type: "VendaItemBPe",
            sale_item_id: 12583626,
            type: "tariff",
            value: 112.29
          },
          {
            id: 313592,
            sale_item_type: "VendaItemBPe",
            sale_item_id: 12583626,
            type: "boarding_fee",
            value: 7.49
          }
        ],
        statuses: [
          {
            id: 68459,
            sale_item_type: "VendaItemBPe",
            sale_item_id: 12583626,
            status: "confirmed",
            created_at: "2022-08-03 18:06:02"
          }
        ]
      }
    ],
    payments: [
      {
        id: 1694364,
        sale_id: 86687990,
        value: 162.19,
        type: "PIX",
        item: {
          id: 1694364,
          payment_id: 1694364,
          operator: "credit_card",
          payment_voucher: "EUMAIS86687990|c57d0de1-b5e6-4b4d-b667-7d7363c83815",
          date_time_transaction: "",
          created_at: "2022-08-03 18:06:01"
        },
        statuses: [
          {
            id: 904695,
            payment_id: 1694364,
            status: "Confirmado",
            created_at: "2022-08-03 18:06:23"
          },
          {
            id: 904693,
            payment_id: 1694364,
            status: "Pendente",
            created_at: "2022-08-03 18:06:01"
          }
        ],
        pix: {
          base64: "",
          copy_paste: "",
          expiration_at: ""
        }
      }
    ]
  }
