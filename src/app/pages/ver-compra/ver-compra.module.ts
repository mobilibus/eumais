import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { VerCompraPageRoutingModule } from './ver-compra-routing.module';

import { VerCompraPage } from './ver-compra.page';
import { ComponentesModule } from 'src/app/componentes/componentes.module';
import { QRCodeModule } from 'angularx-qrcode';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    VerCompraPageRoutingModule,
    ComponentesModule,
    QRCodeModule
  ],
  declarations: [VerCompraPage]
})
export class VerCompraPageModule {}
