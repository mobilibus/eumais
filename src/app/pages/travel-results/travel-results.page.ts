import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { LoadingProvider } from 'src/app/componentes/utils/loading.utils';
import { ApiEucatur } from 'src/app/services/api-euca.service';
import { TripDataService } from 'src/app/services/trip-data.service';
import { environment } from 'src/environments/environment';

@Component({
  selector: 'app-travel-results',
  templateUrl: './travel-results.page.html',
  styleUrls: ['./travel-results.page.scss'],
})
export class TravelResultsPage implements OnInit {

  // Variáveis
  originCityId: number;
  destinyCityId: number;
  departureDate: string;
  returnDate: string;
  cabinId: number;
  adults: number;
  sectionals: any = {};
  travelsLoaded: any = [];
  travelsTreateads: any = [];
  filter = 0;

  constructor(
    private loading: LoadingProvider,
    private activatedRoute: ActivatedRoute,
    private API: ApiEucatur,
    private route: Router,
    public tripData: TripDataService
  ) { }

  ngOnInit() {
    this.loading.showLoading();
    this.activatedRoute.params
      .subscribe(params =>{
        this.originCityId = params.origin_city_id;
        this.destinyCityId = params.destiny_city_id;
        this.departureDate = params.departure_date;
        this.returnDate = params.return_date;
        this.cabinId = params.cabin_id;
        this.adults = params.adults;
      });
      this.searchSecFrom();
  }

  searchSecFrom(){
    if (!this.sectionals.from){
      this.API.getData(`sectionals?id=${this.originCityId}&is_road_station=true`)
        .then((response)=>{
          if (!environment.production){
            console.log(response);
          }

          this.sectionals.from = response[0];
          this.searchSecTo();
        });
    }
  }

  searchSecTo(){
    if (!this.sectionals.to){
      this.API.getData(`sectionals?id=${this.destinyCityId}&is_road_station=true`)
        .then((response)=>{
          this.sectionals.to = response[0];
          setTimeout(() => {
            if (!environment.production){
              console.log(this.sectionals);
            }
            this.searchTravels();
          }, 500);
        });
    }
  }

  searchTravels(){
    this.API.getData(`road/travels/search?origin_sectional_id=${this.originCityId}&destiny_sectional_id=${this.destinyCityId}&departure_date=${this.departureDate}`)
      .then((response)=>{
        this.travelsLoaded = response;
        this.dateTreatment();
      });
  }

  dateTreatment(){
    const weekDays = ['Dom', 'Seg', 'Ter', 'Qua', 'Qui', 'Sex', 'Sáb'];
    const yearMonths = ['Jan', 'Fev', 'Mar', 'Abr', 'Mai', 'Jun', 'Jul', 'Ago', 'Set', 'Out', 'Nov', 'Dez'];
    this.travelsLoaded.forEach(travel => {
      const dayOfMonthDeparture = travel.datetime_departure.substr(8,2);
      const monthDeparture = travel.datetime_departure.substr(5,2);
      const yearDeparture = travel.datetime_departure.substr(0,4);
      const hourDeparture = travel.datetime_departure.substr(11,2);
      const minuteDeparture = travel.datetime_departure.substr(14,2);
      const dateDeparture = new Date(yearDeparture, monthDeparture-1, dayOfMonthDeparture);
      const weekDayDeparture = dateDeparture.getDay();
      const dayOfMonthArrival = travel.datetime_arrival.substr(8,2);
      const monthArrival = travel.datetime_arrival.substr(5,2);
      const yearArrival = travel.datetime_arrival.substr(0,4);
      const hourArrival = travel.datetime_arrival.substr(11,2);
      const minuteArrival = travel.datetime_arrival.substr(14,2);
      const dateArrival = new Date(yearArrival, monthArrival-1, dayOfMonthArrival);
      const weekDayArrival = dateArrival.getDay();
      travel.datePTDeparture = `${dayOfMonthDeparture}/${monthDeparture}/${yearDeparture}`;
      travel.dateUSDeparture = `${yearDeparture}-${monthDeparture}-${dayOfMonthDeparture}`;
      travel.departureTime = `${hourDeparture}:${minuteDeparture}`;
      travel.weekDayDeparture = weekDays[weekDayDeparture];
      travel.yearMonthDeparture = yearMonths[monthDeparture-1];
      travel.datePTArrival = `${dayOfMonthArrival}/${monthArrival}/${yearArrival}`;
      travel.dateUSArrival = `${yearArrival}-${monthArrival}-${dayOfMonthArrival}`;
      travel.arrivalTime = `${hourArrival}:${minuteArrival}`;
      travel.weekDayArrival = weekDays[weekDayArrival];
      travel.yearMonthArrival = yearMonths[monthArrival-1];
      travel.travelTime = Math.round((travel.duration/60));
    });
    setTimeout(() => {
      this.loading.closeLoading();
    }, 500);
    if (!environment.production){
      console.log(this.travelsLoaded);
    }

  }

  sortTravelResults(){
    console.log(this.filter);
    switch (this.filter){
      case 0: {
        this.travelsLoaded = this.travelsLoaded.sort((a, b) => {
          if (a.datetime_departure < b.datetime_departure) return -1;
        });
        break;
      }
      case 1: {
        this.travelsLoaded = this.travelsLoaded.sort((a, b) => {
          if (a.price < b.price) return -1;
        });
        break;
      }
      case 2: {
        this.travelsLoaded = this.travelsLoaded.sort((a, b) => {
          if (b.price < a.price) return -1;
        });
        break;
      }
    }
    console.log(this.travelsLoaded);
  }

  goToTrip(travel){
    this.tripData.tripHash = travel.key;
    this.tripData.tripPassengersCount = this.adults;
    this.tripData.tripDates = { ida: this.departureDate, volta: this.returnDate, idaHorario: travel.departureTime };
    this.tripData.tripWithReturn = this.returnDate !== undefined;
    this.route.navigate([`/seating-map/${travel.key}/${this.adults}`]);
  }

  returnDiscount(promPrice, price){
    let discountPercent = (price/promPrice).toString().substr(2,2);
    discountPercent = discountPercent.substr(0, 1) == '0' ? discountPercent.substr(1, 1) : discountPercent;
    return discountPercent;
  }

  isNumber(str) {
    return !isNaN(str);
  }

}
