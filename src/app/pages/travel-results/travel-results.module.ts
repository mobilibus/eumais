import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { TravelResultsPageRoutingModule } from './travel-results-routing.module';

import { TravelResultsPage } from './travel-results.page';
import { ComponentesModule } from 'src/app/componentes/componentes.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    TravelResultsPageRoutingModule,
    ComponentesModule
  ],
  declarations: [TravelResultsPage]
})
export class TravelResultsPageModule {}
