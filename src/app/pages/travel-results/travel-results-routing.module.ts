import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { TravelResultsPage } from './travel-results.page';

const routes: Routes = [
  {
    path: '',
    component: TravelResultsPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class TravelResultsPageRoutingModule {}
