import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { IonItemSliding } from '@ionic/angular';
import { AlertProvider } from 'src/app/componentes/utils/alert.utils';
import { ToastProvider } from 'src/app/componentes/utils/toast.utils';
import { ApiEucatur } from 'src/app/services/api-euca.service';
import { CartService } from 'src/app/services/cart.service';
import { TripDataService } from 'src/app/services/trip-data.service';
import { environment } from 'src/environments/environment';

@Component({
  selector: 'app-carrinho',
  templateUrl: './carrinho.page.html',
  styleUrls: ['./carrinho.page.scss'],
})
export class CarrinhoPage implements OnInit {
  total = 0;

  constructor(
    public cartService: CartService,
    public tripData: TripDataService,
    private alert: AlertProvider,
    private toast: ToastProvider,
    private apiEuca: ApiEucatur,
    private route: Router
  ) { }

  ngOnInit() {
    if (!environment.production){
      console.log(this.cartService.items);
      console.log(this.tripData);
    }
    this.calcTotal();
    this.tripData.cleanData();
  }

  calcTotal(){
    if (this.cartService.items.length > 0){
      this.cartService.items.map(item =>{
        this.total += item.tripInfo.seatInfo.amount;
      });
    } else {
      this.total = 0;
    }
  }

  removerItem(item, index){
    if (!environment.production){
      console.log(item);
    }


    this.alert.showAlert('Tem certeza?', '', 'Deseja realmente remover este item de seu carrinho?', [
      {
        text: 'Sim',
        handler: ()=>{
          this.apiEuca.deleteData(`road/travels/${item.tripInfo.tripHash}/selected_seats/${item.road.selected_seat_key}`)
            .then((res)=>{
              if (!environment.production){
                console.log(res);
              }
              this.cartService.items.splice(index);
              this.calcTotal();
              this.toast.showToast(`Item removido com sucesso`);
            })
            .catch((err)=>{
              if (!environment.production){
                console.log(err);
              }
            });
        }
      },
      'Não'
    ]);
  }
  efetuarPgto(){
    this.route.navigate([`/pgto-um/${this.total}`]);
  }

  selectReturnTrip(item){
    // eslint-disable-next-line max-len
    this.tripData.tripSectionals = {
      from: {
        id: item.tripInfo.tripSectionals.to.id,
        code: item.tripInfo.tripSectionals.to.code,
        description: item.tripInfo.tripSectionals.to.description,
        // eslint-disable-next-line @typescript-eslint/naming-convention
        uf_acronym: item.tripInfo.tripSectionals.to.uf_acronym,
      },
      to: {
        id: item.tripInfo.tripSectionals.from.id,
        code: item.tripInfo.tripSectionals.from.code,
        description: item.tripInfo.tripSectionals.from.description,
        // eslint-disable-next-line @typescript-eslint/naming-convention
        uf_acronym: item.tripInfo.tripSectionals.from.uf_acronym,
      }
    };
    this.route.navigate([`/travel-results/${item.tripInfo.tripSectionals.to.id}/${item.tripInfo.tripSectionals.from.id}/2/${item.tripInfo.tripPassengersCount}/${item.tripInfo.tripDates.volta}`]);
  }

  dateTreat(date: string, type: string = 'BRL'){
    const year = date.substring(0, 4);
    const month = date.substring(5, 7);
    const days = date.substring(8,10);
    return `${days}/${month}/${year}`;
  }

  showDetails(item: IonItemSliding){
    item.open('end');
  }


}
