import { Component, OnInit, ViewChild } from '@angular/core';
import { IonItemSliding } from '@ionic/angular';
import { AlertProvider } from 'src/app/componentes/utils/alert.utils';
import { LoadingProvider } from 'src/app/componentes/utils/loading.utils';
import { ApiEucatur } from 'src/app/services/api-euca.service';
import { UtilsService } from 'src/app/services/utils.service';
import { environment } from 'src/environments/environment';

@Component({
  selector: 'app-enderecos',
  templateUrl: './enderecos.page.html',
  styleUrls: ['./enderecos.page.scss'],
})
export class EnderecosPage implements OnInit {
  @ViewChild('cepInput') cepInput: HTMLInputElement;
  listAddress: any = [];
  listCountries: any = [];
  showNewAddress: boolean = false;
  textEndereco: string;
  textNumero: string;
  textComplemento: string;
  textCidade: string;
  textBairro: string;
  textUF: string;
  textCep: string;
  textPais: string;
  textPadrao: boolean = false;
  openedItems: any = [];

  constructor(
    private apiEuca: ApiEucatur,
    private utils: UtilsService,
    private alert: AlertProvider,
    private loading: LoadingProvider
  ) { }

  ngOnInit() {
    this.listarEnderecos();
    this.listarPaises();
  }

  listarEnderecos(){
    this.loading.showLoading();
    this.apiEuca.getData('person/address')
      .then((res:any)=>{
        if (!environment.production){
          console.log(res);
        }
        this.listAddress = res;
        setTimeout(() => {
          this.loading.closeLoading();
        }, 150);
      })
      .catch((err:any)=>{
        if (err.status == 400 || err.status == 404){
          this.listAddress = [];
          setTimeout(() => {
            this.loading.closeLoading();
          }, 150);
        }
      })
  }

  toggleNewAddress(){
    if (this.showNewAddress){
      this.showNewAddress = false;
    } else {
      this.showNewAddress = true;
    }
  }

  listarPaises(){
    this.utils.generalGetApi('https://servicodados.ibge.gov.br/api/v1/paises/')
      .then((res:any)=>{
        this.listCountries = res.map(obj => obj.nome.abreviado);
      })
  }

  consultaCEP(){
    this.loading.showLoading();
    this.utils.generalGetApi(`https://viacep.com.br/ws/${this.textCep}/json`)
    .then((res:any)=>{
      this.textCidade = res.localidade;
      this.textComplemento = res.complemento;
      this.textEndereco = res.logradouro;
      this.textBairro = res.bairro;
      this.textUF = res.uf;
      this.textPais = 'Brasil';
      setTimeout(() => {
        this.loading.closeLoading();
      }, 300);
      if (!environment.production){
        console.log(res);
      }
    });
  }

  cadastrarEndereco(){
    if (this.textEndereco == undefined || this.textEndereco == ''){
      this.alert.showAlert('Oops', '', 'Você esqueceu de informar seu endereço.', ['Ok']);
    } else if (this.textCidade == undefined || this.textCidade == ''){
      this.alert.showAlert('Oops', '', 'Você esqueceu de informar a cidade .', ['Ok']);
    } else if (this.textBairro == undefined || this.textBairro == ''){
      this.alert.showAlert('Oops', '', 'Você esqueceu de informar a cidade .', ['Ok']);
    } else if (this.textUF == undefined || this.textUF == ''){
      this.alert.showAlert('Oops', '', 'Você esqueceu de informar o UF.', ['Ok']);
    } else if (this.textCep == undefined || this.textCep == ''){
      this.alert.showAlert('Oops', '', 'Você esqueceu de informar CEP de seu endereço.', ['Ok']);
    } else if (this.textPais == undefined || this.textPais == ''){
      this.alert.showAlert('Oops', '', 'Você esqueceu de informar o país de seu endereço.', ['Ok']);
    } else {
      this.loading.showLoading();
      let body = {
        address: this.textEndereco,
        city: this.textCidade,
        complement: this.textComplemento,
        country: this.textPais,
        is_default: this.textPadrao,
        neighborhood: this.textBairro,
        number: this.textNumero.toString(),
        uf: this.textUF,
        zip_code: this.textCep.toString()
      };

      if (!environment.production){
        console.log(body);
      }

      this.apiEuca.postData('person/address', body)
        .then((res:any)=>{
          this.loading.closeLoading();
          setTimeout(() => {
            this.alert.showAlert('Sucesso!', '', 'Endereço cadastrado com sucesso', ['Continuar'])
          }, 100);

          this.listarEnderecos();
          this.showNewAddress = false;
        })
    }
  }

  excluirEndereco(id){
    this.alert.showAlert('Ei, atenção...', '', 'Você realmente deseja excluir este endereço?',
    [
      {
        text: 'Sim',
        handler:()=>{
          this.loading.showLoading();
          this.apiEuca.deleteData(`person/address/${id}`)
            .then((res:any)=>{
              this.loading.closeLoading();

              if (!environment.production){
                console.log(res);
              }

              setTimeout(() => {
                this.alert.showAlert('Excluído', '', 'Endereço excluído com sucesso!', ['Ok']);
              }, 100);
              this.listarEnderecos();
            });
        }
      },
      'Não'
    ])
  }

  definirPadrao(end){
    this.alert.showAlert('Definir Padrão?', '', `Você realmente deseja definir ${end.address}, ${end.city}, ${end.uf} como seu endereço padrão?`,
    [
      {
        text: 'Sim',
        handler:()=>{
          this.loading.showLoading();
          let body = {
            address: end.address,
            city: end.city,
            complement: end.complement,
            country: end.country,
            is_default: true,
            neighborhood: end.neighborhood,
            number: end.number.toString(),
            uf: end.uf,
            zip_code: end.zip_code.toString()
          };

          this.apiEuca.editData(`person/address/${end.id}`, body)
            .then((res:any)=>{
              this.loading.closeLoading();

              if (!environment.production){
                console.log(res);
              }

              setTimeout(() => {
                this.alert.showAlert('Pronto!', '', 'Endereço definido como padrão!', ['Ok']);
              }, 100);
              this.listarEnderecos();
            });
        }
      },
      'Não'
    ])
  }

  showDetails(item: IonItemSliding){
    item.open('end');
  }

}
