import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { IonicModule } from '@ionic/angular';
import { PerfilPageRoutingModule } from './perfil-routing.module';
import { PerfilPage } from './perfil.page';
import { ComponentesModule } from 'src/app/componentes/componentes.module';
import { PerfilModalComponent } from './perfil.modal';


@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    PerfilPageRoutingModule,
    ComponentesModule
  ],
  declarations: [PerfilPage, PerfilModalComponent],
})
export class PerfilPageModule {}
