import { CoreEnvironment } from '@angular/compiler/src/compiler_facade_interface';
import { Component } from '@angular/core';

import { ModalController } from '@ionic/angular';
import { environment } from 'src/environments/environment';

@Component({
  selector: 'app-perfil-modal',
  templateUrl: 'perfil.modal.html',
})
export class PerfilModalComponent {
  telSupport = environment.telSupport;
  wppSupport = environment.wppSupport;
  emailSupport = environment.emailSupport;

  constructor(private modalCtrl: ModalController) {}

  close() {
    return this.modalCtrl.dismiss(null, 'cancel');
  }
}
