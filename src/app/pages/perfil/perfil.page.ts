import { Component, OnInit, ViewChild } from '@angular/core';
import { Router } from '@angular/router';
import { AlertProvider } from 'src/app/componentes/utils/alert.utils';
import { StorageService } from 'src/app/services/storage.service';
import { InAppBrowser } from '@awesome-cordova-plugins/in-app-browser/ngx';
import { environment } from 'src/environments/environment';
import { AuthService } from 'ionic-appauth';
import { CartService } from 'src/app/services/cart.service';
import { ModalController } from '@ionic/angular';
import { PerfilModalComponent } from './perfil.modal';


@Component({
  selector: 'app-perfil',
  templateUrl: './perfil.page.html',
  styleUrls: ['./perfil.page.scss'],
})
export class PerfilPage implements OnInit {
  commonsItems: any[] = [
    { icon: 'cart-outline', label: 'Carrinho', color: 'tertiary', link: '/carrinho' },
    { icon: 'map-outline', label: 'Endereços', color: 'tertiary', link: '/enderecos' },
    { icon: 'card-outline', label: 'Cartões', color: 'tertiary', link: '/cartoes' },
    { icon: 'ticket-outline', label: 'Compras', color: 'tertiary', link: '/compras' },
  ]
  helpItems: any[] = [
    // { icon: 'help-buoy', label: 'F.A.Q', color: 'tertiary', link: '/faq' },
    // { icon: 'help', label: 'Sobre', color: 'tertiary', link: '/sobre' },

  ]
  externalItems: any[] = [
    { icon: 'document-text', label: 'Regulamento', color: 'tertiary', url: 'http://www.eumaisviagens.com.br/regulamento/' },
    { icon: 'eye', label: 'Privacidade', color: 'tertiary', url: 'http://www.eumaisviagens.com.br/termos-de-privacidade/' }
  ]
  userData: any;
  actualWebView: any;
  appVersion: string = environment.version;
  appStage: string = environment.production ? "prod" : "dev"

  constructor(
    private auth: AuthService,
    private storage: StorageService,
    private alert: AlertProvider,
    private route: Router,
    private iab: InAppBrowser,
    private cart: CartService,
    private modalCtrl: ModalController
  ) { }

  ngOnInit() {
    this.userData = this.storage.getItem(this.storage.userKey, true);
  }

  reloadUserData(){
    this.userData = this.storage.getItem(this.storage.userKey, true);
  }

  doLogout(){
    this.alert.showAlert('Tem certeza?', '', 'Você realmente deseja sair desta conta?',
    [
      {
        text: 'Sim',
        handler: (val:any)=>{
          this.auth.signOut();
          setTimeout(() => {
            this.userData = null;
            this.storage.removeItem(this.storage.userKey);
            this.cart.cleanCart();
            this.route.navigate(['/home']);
          }, 300);
        }
      },
      'Não'
    ]);
  }

  openExternalLink(link){
    this.iab.create(link, '_self', 'hideurlbar=true,hidenavigationbuttons=true').show();
  }

  async openModal() {
    const modal = await this.modalCtrl.create({
      component: PerfilModalComponent,
    });
    modal.present();

    const { data, role } = await modal.onWillDismiss();
  }

}
