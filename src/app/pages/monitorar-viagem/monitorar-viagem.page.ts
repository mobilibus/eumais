import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { CapacitorGoogleMaps } from '@capacitor-community/capacitor-googlemaps-native';
import { LoadingProvider } from 'src/app/componentes/utils/loading.utils';
import { ApiEucatur } from 'src/app/services/api-euca.service';
import { environment } from 'src/environments/environment';

@Component({
  selector: 'app-monitorar-viagem',
  templateUrl: './monitorar-viagem.page.html',
  styleUrls: ['./monitorar-viagem.page.scss'],
})
export class MonitorarViagemPage implements OnInit {
  code: number = null;
  markers: any[] = [];
  realtimeData: any = null;

  @ViewChild('map') mapView: ElementRef;
  constructor(
    private apiEucatur: ApiEucatur,
    private activatedRoute: ActivatedRoute,
    private loading: LoadingProvider
  ) { }


  ngOnInit() {
    this.loading.showLoading();
    this.activatedRoute.params
      .subscribe(params =>{
        if (params.code){
          this.code = params.code;
        }
      });

  }

  ionViewDidEnter(){
    this.createMap();
  }

  ionViewDidLeave(){
    this.markers = [];
    CapacitorGoogleMaps.clear();
    CapacitorGoogleMaps.close();
  }

  createMap(){
    const boundingRect = this.mapView.nativeElement.getBoundingClientRect() as DOMRect;
    this.mapView.nativeElement.style.zIndex = "-1";

    const mapView = CapacitorGoogleMaps.create({
      width: Math.round(boundingRect.width),
      height: Math.round(boundingRect.height),
      x: Math.round(boundingRect.x),
      y: Math.round(boundingRect.y),
      latitude: -26.9205478,
      longitude: -49.3692149,
      zoom: 11,
      liteMode: false
    });

    CapacitorGoogleMaps.addListener('onMapReady', async ()=>{
      this.getTripData();
      this.mapLaunchOpts();
    });
  }

  getTripData(){
    let endpoint = '';
    if (!this.code) {
      endpoint = 'proxys/get-real-time'
    } else {
      endpoint = `proxys/get-real-time/${this.code}`
    }

    this.apiEucatur.tripMonitoring(endpoint)
      .then((res)=>{
        if (this.code) {
          if (!environment.production) console.log(res);
          this.realtimeData = res;
          this.createMarker('Ônibus', res.lat, res.lng, `${res.ordem} | ${res.descricaoLinha}`);
          setTimeout(() => {
            this.recenterMap(res.lat, res.lng);
            this.createPolyline();
            this.loading.closeLoading();
          }, 30);
        }
      });
  }


  createMarker(name: string, lat: number, lng: number, title: string, icon?: string){
    this.markers.push({
      name: name,
      lat: lat,
      lng: lng,
      title: title,
      icon: icon || environment.gMaps.defaultBusIcon,
    })
    setTimeout(() => {
      this.updateMarkers();
    }, 30);
  }

  recenterMap(lat:number, lng:number){
    CapacitorGoogleMaps.setCamera({
      latitude: lat,
      longitude: lng,
    })
  }

  async updateMarkers(){
    this.markers.map(async (marker)=>{
      CapacitorGoogleMaps.addMarker({
        latitude: marker.lat,
        longitude: marker.lng,
        iconUrl: marker.icon,
        title: marker.title,
      });
    });
  }

  createPolyline(){
    //to do
  }

  mapLaunchOpts() {
    CapacitorGoogleMaps.setMapStyle({
      jsonString: environment.gMaps.mapStyle
    });
    CapacitorGoogleMaps.settings({
      rotateGestures: false,
      compassButton: false,
      myLocationButton: false
    });
    CapacitorGoogleMaps.setTrafficEnabled({
      enabled: true
    })

  }

  dateFormatter(str: string){
    return `${str.substr(0,2)}/${str.substr(3,2)}/${str.substr(6,4)} - ${str.substr(11,5)}`;
  }

}
