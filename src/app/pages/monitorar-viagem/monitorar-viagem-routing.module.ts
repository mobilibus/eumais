import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { MonitorarViagemPage } from './monitorar-viagem.page';

const routes: Routes = [
  {
    path: '',
    component: MonitorarViagemPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class MonitorarViagemPageRoutingModule {}
