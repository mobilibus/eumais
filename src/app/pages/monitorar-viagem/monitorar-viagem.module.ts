import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { IonicModule } from '@ionic/angular';
import { MonitorarViagemPageRoutingModule } from './monitorar-viagem-routing.module';
import { MonitorarViagemPage } from './monitorar-viagem.page';
import { ComponentesModule } from 'src/app/componentes/componentes.module';


@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    MonitorarViagemPageRoutingModule,
    ComponentesModule
  ],
  declarations: [MonitorarViagemPage],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class MonitorarViagemPageModule {}
