import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';
import { AuthGuardService } from './core/guards/auth.guard';

const routes: Routes = [
  { path: '', pathMatch: 'full',  redirectTo: 'splash'},
  { path: 'home', loadChildren: () => import('./pages/home/home.module').then( m => m.HomePageModule) },
  { path: 'travel-results', loadChildren: () => import('./pages/travel-results/travel-results.module').then( m => m.TravelResultsPageModule) },
  { path: 'travel-results/:origin_city_id/:destiny_city_id/:cabin_id/:adults/:departure_date/:return_date', loadChildren: () => import('./pages/travel-results/travel-results.module').then( m => m.TravelResultsPageModule) },
  { path: 'travel-results/:origin_city_id/:destiny_city_id/:cabin_id/:adults/:departure_date', loadChildren: () => import('./pages/travel-results/travel-results.module').then( m => m.TravelResultsPageModule) },
  { path: 'seating-map', loadChildren: () => import('./pages/seating-map/seating-map.module').then( m => m.SeatingMapPageModule) },
  { path: 'seating-map/:travel_key/:adults', loadChildren: () => import('./pages/seating-map/seating-map.module').then( m => m.SeatingMapPageModule) },
  { path: 'login', loadChildren: () => import('./pages/login/login.module').then( m => m.LoginPageModule) },
  { path: 'person-to-seat', loadChildren: () => import('./pages/person-to-seat/person-to-seat.module').then( m => m.PersonToSeatPageModule) },
  { path: 'person-to-seat/:number/:type', loadChildren: () => import('./pages/person-to-seat/person-to-seat.module').then( m => m.PersonToSeatPageModule) },
  { path: 'faq', loadChildren: () => import('./pages/bottom-tab/faq/faq.module').then( m => m.FaqPageModule) },
  { path: 'perfil', loadChildren: () => import('./pages/perfil/perfil.module').then( m => m.PerfilPageModule), canActivate: [AuthGuardService] },
  { path: 'carrinho', loadChildren: () => import('./pages/carrinho/carrinho.module').then( m => m.CarrinhoPageModule), canActivate: [AuthGuardService] },
  { path: 'enderecos', loadChildren: () => import('./pages/enderecos/enderecos.module').then( m => m.EnderecosPageModule), canActivate: [AuthGuardService] },
  { path: 'cartoes', loadChildren: () => import('./pages/cartoes/cartoes.module').then( m => m.CartoesPageModule), canActivate: [AuthGuardService] },
  { path: 'cartoes/:showCad', loadChildren: () => import('./pages/cartoes/cartoes.module').then( m => m.CartoesPageModule), canActivate: [AuthGuardService] },
  { path: 'full-view', loadChildren: () => import('./pages/full-view/full-view.module').then( m => m.FullViewPageModule) },
  { path: 'pgto-um', loadChildren: () => import('./pages/pgto-um/pgto-um.module').then( m => m.PgtoUmPageModule) },
  { path: 'pgto-um/:subtotal', loadChildren: () => import('./pages/pgto-um/pgto-um.module').then( m => m.PgtoUmPageModule) },
  { path: 'pgto-concluido', loadChildren: () => import('./pages/pgto-concluido/pgto-concluido.module').then( m => m.PgtoConcluidoPageModule) },
  { path: 'pgto-concluido/:sale_id', loadChildren: () => import('./pages/pgto-concluido/pgto-concluido.module').then( m => m.PgtoConcluidoPageModule) },
  { path: 'pgto-recusado', loadChildren: () => import('./pages/pgto-recusado/pgto-recusado.module').then( m => m.PgtoRecusadoPageModule) },
  { path: 'compras', loadChildren: () => import('./pages/compras/compras.module').then( m => m.ComprasPageModule) },
  { path: 'ver-compra', loadChildren: () => import('./pages/ver-compra/ver-compra.module').then( m => m.VerCompraPageModule) },
  { path: 'ver-compra/:saleId', loadChildren: () => import('./pages/ver-compra/ver-compra.module').then( m => m.VerCompraPageModule) },
  { path: 'ver-compra/:saleId/:origin', loadChildren: () => import('./pages/ver-compra/ver-compra.module').then( m => m.VerCompraPageModule) },
  { path: 'splash', loadChildren: () => import('./pages/splash/splash.module').then( m => m.SplashPageModule) },
  { path: 'monitorar-viagem', loadChildren: () => import('./pages/monitorar-viagem/monitorar-viagem.module').then( m => m.MonitorarViagemPageModule) },
  { path: 'monitorar-viagem/:code', loadChildren: () => import('./pages/monitorar-viagem/monitorar-viagem.module').then( m => m.MonitorarViagemPageModule) },
  { path: 'auth/callback', loadChildren: ()=> import('./auth/auth-callback/auth-callback-routing.module').then( m => m.AuthCallbackPageRoutingModule) },
  { path: 'auth/endsession', loadChildren: ()=> import('./auth/auth-callback/auth-callback-routing.module').then( m => m.AuthCallbackPageRoutingModule) },


];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule { }
