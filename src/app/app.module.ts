import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { RouteReuseStrategy } from '@angular/router';
import { IonicModule, IonicRouteStrategy } from '@ionic/angular';
import { AppComponent } from './app.component';
import { AppRoutingModule } from './app-routing.module';
import { ApiEucatur } from './services/api-euca.service';
import { HttpClientModule } from '@angular/common/http';
import { AlertProvider } from './componentes/utils/alert.utils';
import { ToastProvider } from './componentes/utils/toast.utils';
import { LoadingProvider } from './componentes/utils/loading.utils';
import { NgxMaskIonicModule } from 'ngx-mask-ionic';
import { NgCalendarModule } from 'ionic2-calendar';
import { LoadingPaymentProvider } from './componentes/utils/loading-payment.utils';
import { QRCodeModule } from 'angularx-qrcode';
import { InAppBrowser } from '@awesome-cordova-plugins/in-app-browser/ngx';
import { CoreModule } from './core/core.module';
import { Clipboard } from '@ionic-native/clipboard/ngx';




@NgModule({
    declarations: [AppComponent],
    imports: [
        BrowserModule,
        NgCalendarModule,
        IonicModule.forRoot(),
        AppRoutingModule,
        HttpClientModule,
        NgxMaskIonicModule.forRoot(),
        QRCodeModule,
        CoreModule
    ],
    providers: [
        { provide: RouteReuseStrategy, useClass: IonicRouteStrategy },
        ApiEucatur,
        AlertProvider,
        ToastProvider,
        LoadingProvider,
        LoadingPaymentProvider,
        InAppBrowser,
        Clipboard
    ],
    bootstrap: [AppComponent]
})
export class AppModule {}
